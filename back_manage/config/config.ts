import { defineConfig } from 'umi';
import route from './route';
import MonacoWebpackPlugin from 'monaco-editor-webpack-plugin';

const chainWebpack = (config: any) => {
  config.plugin('monaco-editor').use(MonacoWebpackPlugin, [
    {
      languages: ['json','markdown'],
    },
  ]);
};
export default defineConfig({
  layout: {
    name: '管理后台',
    layout: 'side',
  },
  routes: route,
  proxy: {
    '/api': {
      target: 'https://creationapi.shbwyz.com',
      changeOrigin: true,
      pathRewrite: {
        '^/api': '',
      },
    },
  },
  dva: {
    immer: true,
    hmr: false,
  },
  chainWebpack,
});
