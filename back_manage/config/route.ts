const route = [
  {
    path: '/login',
    component: '@/pages/login',
    menuRender: false,
    headerRender: false,
    footerRender: false
  },
  {
    path: '/register',
    component: '@/pages/register',
    menuRender: false,
    headerRender: false,
  },
  {
    path: '/',
    redirect: '/login',
  },

  {
    path: '/work',
    component: '@/pages/work',
    name: '工作台',
    icon: 'dashboard',
  },
  {
    name: '文章管理',
    path: '/article',
    icon: 'form',
    routes: [
      {
        path: '/article',
        redirect: '/article/article',
      },
      {
        path: '/article/article',
        component: '@/pages/article',
        name: '所有文章',
        icon: 'form',
      },
      {
        path: '/article/category',
        component: '@/pages/category',
        name: '分类管理',
        icon: 'copy',
      },
      {
        path: '/article/tags',
        component: '@/pages/tags',
        name: '标签管理',
        icon: 'tag',
      },
      {
        path: '/article/editor',
        component: '@/pages/articleEdit',
        menuRender: false,
        headerRender: false,
        footerRender: false
      },
      {
        path: '/article/editor/:id',
        component: '@/pages/articleEdit',
        menuRender: false,
        headerRender: false,
        footerRender: false
      },
      {
        path: '/article/amEditor',
        component: '@/pages/articleAmEditor',
        menuRender: false,
        headerRender: false,
        footerRender: false
      },
    ],
  },
  {
    path: '/page',
    component: '@/pages/page',
    name: '页面管理',
    icon: 'snippets',
  },
  {
    path: '/knowledge',
    component: '@/pages/knowledgeBooks',
    name: '知识小册',
    icon: 'book',
  },
  {
    path: '/poster',
    component: '@/pages/poster',
    name: '海报管理',
    icon: 'star',
  },
  {
    path: '/comment',
    component: '@/pages/comment',
    name: '评论管理',
    icon: 'message',
  },
  {
    path: '/email',
    component: '@/pages/email',
    name: '邮件管理',
    icon: 'mail',
  },
  {
    path: '/file',
    component: '@/pages/file',
    name: '文件管理',
    icon: 'folder-open',
  },
  {
    path: '/search',
    component: '@/pages/search',
    name: '搜索记录',
    icon: 'search',
  },
  {
    path: '/visit',
    component: '@/pages/visit',
    name: '访问记录',
    icon: 'project',
  },
  {
    path: '/user',
    component: '@/pages/user',
    name: '用户管理',
    icon: 'user',
    access: 'isAdmin',
  },
  {
    path: '/system',
    component: '@/pages/system',
    name: '系统设置',
    icon: 'setting',
  },
  { path: '/myCenter', component: '@/pages/myCenter' },
  {
    path: '/page/editor',
    component: '@/pages/pageEdit',
    menuRender: false,
    headerRender: false,
  },
  {
    path: '/page/editor/:id',
    component: '@/pages/pageEdit',
    menuRender: false,
    headerRender: false,
  },
  {
    path: '/page/editors/:id',  //测试路由
    component: '@/pages/page/editor',
    menuRender: false,
    headerRender: false,
    footerRender: false
  },
  {
    path: '/knowledge/editor/:id',
    component: '@/pages/knowledgeEdit',
    menuRender: false,
    headerRender: false,
    footerRender: false
  },
];
export default route;
