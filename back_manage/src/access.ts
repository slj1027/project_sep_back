export default function (initState: any) {
  // console.log(initState,111111111);
  const { role } = initState;
  
  return {
    isAdmin: () => role === 'admin',
  };
}
