import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime'; //引入展示相对时间的插件
import updateLocale from 'dayjs/plugin/updateLocale'; //更新语言配置
import localizedFormat from 'dayjs/plugin/localizedFormat';
import 'dayjs/locale/zh-cn'; //导入本地化语言
dayjs.locale('zh-cn'); //配置语言的加载环境
dayjs.extend(updateLocale); //依赖
dayjs.extend(relativeTime); //依赖
dayjs.extend(localizedFormat);
export const time = (createTime: any) => `${dayjs(createTime).fromNow()}`; //封装时间格式化函数
export const date = (createTime: any) => `${dayjs(createTime).format('L LT')}`;
function repair(m: string | number) {
    return m < 10 ? '0' + m : m
  }
  export function formatting(time: string) {
    var times = new Date(time);
    var y = times.getFullYear();
    var m = times.getMonth() + 1;
    var d = times.getDate();
    var h = times.getHours();
    var mm = times.getMinutes();
    var s = times.getSeconds();
    return y + '-' + repair(m) + '-' + repair(d) + ' ' + repair(h) + ':' + repair(mm) + ':' + repair(s);
  }