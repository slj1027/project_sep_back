import React from 'react';
import { Affix } from 'antd';
import style from './less/index.less';
interface PropType {
  children: JSX.Element;
  offsetTop: number;
}
function AffixHead({ children, offsetTop }: PropType) {
  return (
    <Affix offsetTop={offsetTop}>
      <header className={style.header}>{children}</header>
    </Affix>
  );
}

export default AffixHead;
