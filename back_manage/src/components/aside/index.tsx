import { useState } from 'react';
import { Drawer, Row, Col, Image, Card } from 'antd';
import { ProFormText, QueryFilter } from '@ant-design/pro-components';
import style from './less/index.less';
import Pagination_com from '../pagination_com';
import { useRequest } from 'ahooks';
import { getFile } from '@/api';
const { Meta } = Card;
interface PropsType {
  open: boolean;
  onClose: () => void;
  onCheck: (img: string) => void;
}
function Aside({ onClose, open, onCheck }: PropsType) {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(12);
  const { data, run } = useRequest((params = { page, pageSize }) =>
    getFile(params),
  );
  const [visible, setVisible] = useState(false);
  // 搜索
  const searchHander = async (values: any) => {};
  const onChange = (page: number, pageSize: number) => {
    run({ page, pageSize });
    setPage(page);
    setPageSize(pageSize);
  };
  return (
    <Drawer
      title="文件选择"
      placement="right"
      onClose={onClose}
      open={open}
      size="large"
    >
      <div className={style.file_search}>
        <QueryFilter<{
          name: string;
          company: string;
        }>
          onFinish={searchHander}
        >
          <ProFormText name="name" label="文件名称" />
          <ProFormText name="type" label="文件类型" />
        </QueryFilter>
      </div>
      <div className={style.file_img}>
        {/* 图片渲染 */}
        <Row justify="space-around">
          {(data as any) &&
            (data as any).data &&
            (data as any).data[0] &&
            (data as any).data[0].map((item: any, index: number) => {
              return (
                <Col
                  key={index}
                  xs={{ span: 24 }}
                  sm={{ span: 10 }}
                  md={{ span: 6 }}
                  lg={{ span: 5 }}
                  xl={{ span: 4 }}
                >
                  <Card
                    hoverable
                    style={{ width: '100%' }}
                    cover={
                      <Image
                        preview={{ visible: false }}
                        src={item.url}
                        onClick={() => setVisible(true)}
                        style={{ width: '100%', height: '150px' }}
                      />
                    }
                    onClick={() => {
                      onCheck(item.url);
                      onClose();
                    }}
                  >
                    <Meta title={item.url} />
                  </Card>
                </Col>
              );
            })}
        </Row>
        <div style={{ display: 'none' }}>
          <Image.PreviewGroup
            preview={{
              visible,
              onVisibleChange: (vis) => setVisible(vis),
            }}
          >
            {(data as any) &&
              (data as any).data &&
              (data as any).data[0] &&
              (data as any).data[0].map((item: any, index: number) => {
                return <Image src={item.url} key={index} />;
              })}
          </Image.PreviewGroup>
        </div>
        {/* 分页 */}
        {data && (
          <div className={style.pagination}>
            <Pagination_com
              pageSize={pageSize}
              total={data.data[1]}
              current={page}
              onChange={onChange}
            />
          </div>
        )}
      </div>
    </Drawer>
  );
}

export default Aside;
