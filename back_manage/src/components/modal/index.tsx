import { Button, Modal, Input } from 'antd';
import React, { useState } from 'react';
const { TextArea } = Input;
interface eType {
  target: {
    value: string;
  };
}
interface PropsType {
  isModalOpen: boolean;
  showModal: () => void;
  handleOk: (value: string) => void;
  handleCancel: () => void;
}
function ModalBox({
  isModalOpen,
  showModal,
  handleOk,
  handleCancel,
}: PropsType) {
  const [value, setValue] = useState('');
  return (
    <Modal
      title="回复评论"
      open={isModalOpen}
      onOk={() => handleOk(value)}
      onCancel={handleCancel}
      okText="回复"
    >
      <TextArea onChange={(e) => setValue(e.target.value)}></TextArea>
    </Modal>
  );
}

export default ModalBox;
