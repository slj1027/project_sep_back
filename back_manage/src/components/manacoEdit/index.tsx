import { useEffect, useState } from 'react';
import Monaco from 'react-monaco-editor';
interface PropType {
  code: string;
  changeValue: (value: string, code: string) => void;
}
function MonacoEdit({ code, changeValue }: PropType) {
  console.log(code);

  const [str, setStr] = useState('');
  useEffect(() => {
    if (code) {
      if (typeof code === 'object') {
        code = JSON.stringify(code);
      }

      setStr(code);
    }
  }, [code]);
  return (
    <Monaco
      width="100%"
      height="60vh"
      language="json"
      theme="vs-light"
      value={str}
      options={{ selectOnLineNumbers: true }}
      onChange={(value) => changeValue(value, code)}
    />
  );
}

export default MonacoEdit;
