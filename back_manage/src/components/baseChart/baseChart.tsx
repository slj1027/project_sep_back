import { EChartsType, init, EChartsOption, dispose } from 'echarts';
import { useEffect, useRef } from 'react';
import './less/index.less';
import * as echarts from 'echarts';
type Types = 'pie' | 'bar' | 'line';
interface OptionsType {
  type: Types;
  series: any[];
  xData?: any[];
  yData?: any[];
  chartData?: any;
  color?: any;
}
interface MyProps extends OptionsType {
  width?: number;
  height?: number;
  loading?: boolean;
}
//  格式化数据
const formatOption = ({
  type,
  series,
  xData,
  yData,
  chartData,
}: OptionsType): EChartsOption => {
  switch (type) {
    case 'pie':
      return {
        ...chartData,
        series,
      };
    case 'bar':
    case 'line':
      return {
        ...chartData,
        xAxis: {
          type: 'category',
          data: xData,
        },
        yAxis: {
          type: 'value',
        },
        series,
      };
  }
};

const BaseChart = ({
  type = 'bar',
  series = [], // 图表主数据
  width = 700,
  height = 400,
  xData = [], // x轴数据
  yData = [], // y轴数据
  chartData = {},
  loading = true,
  color = [],
}: MyProps) => {
  const chartInstance = useRef<null | EChartsType>(null);
  const chartDom = useRef<null | HTMLDivElement>(null);
  useEffect(() => {
    let myCharts = echarts.init(chartDom.current! as HTMLElement);
    window.onresize = function () {
      myCharts.resize();
    };

    chartInstance.current = init(chartDom.current as HTMLElement, '', {
      width,
      height,
    });
    return () => {
      chartInstance.current?.dispose(); // 图表和图表内组建的卸载
      chartInstance.current && dispose(chartInstance.current); // 卸载dom所有的方法
    };
  }, []);
  useEffect(() => {
    if (chartInstance.current) {
      chartInstance.current.setOption(
        formatOption({
          type,
          series,
          xData,
          yData,
          color,
          chartData,
        }),
      );
    }
  }, [type, series, xData, yData, chartInstance, color]);
  useEffect(() => {
    if (chartInstance.current) {
      loading
        ? chartInstance.current.showLoading()
        : chartInstance.current.hideLoading();
    }
  }, [loading, chartInstance]);
  return <div ref={chartDom} className="emx"></div>;
};

export default BaseChart;
