import React, { useEffect, useState } from 'react';
import { Button, Drawer, Form, Input, Switch, Space, UploadProps } from 'antd';
import type { DrawerProps } from 'antd/es/drawer';
import style from './knowladge.less';
import { _add_knowladge, _update_knowladge } from '@/api/knowladge';
import Aside from '../aside';
import OssUpload from '../ossUpload';
type Props = {
  open: boolean;
  onClose: () => void;
  type: string;
  id: string;
  obj: any;
  run?: (params: any) => void;
  page?: number;
  pageSize?: number;
};
const Knowladge = (props: Props) => {
  const [placement] = useState<DrawerProps['placement']>('right');

  const { open, onClose, type, id, obj, run, page, pageSize } = props;
  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue({
      ...obj,
    });
  }, [obj]);

  const onFinish = (values: any) => {
    if (type === 'new') {
      _add_knowladge(values).then(() => run && run({ page, pageSize }));
    } else if (type === 'update') {
      _update_knowladge(id, values).then(() => run && run({ page, pageSize }));
    }
    onClose()
  };
  const [open1, setOpen] = useState(false);
  const onCheck = (img: string) => {
    form.setFieldsValue({
      cover: img,
    });
  };
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose1 = () => {
    setOpen(false);
  };
  const changeImg = (img: string) => {
    form.setFieldsValue({
      cover: img,
    });
  };
  return (
    <div>
      <Drawer
        title="更新知识库"
        placement={placement}
        width={600}
        onClose={onClose}
        open={open}
      >
        <Form autoComplete="off" onFinish={onFinish} form={form}>
          <div className={style.mask}>
            <Form.Item name="title" label="名称">
              <Input placeholder="" />
            </Form.Item>
            <Form.Item name="summary" label="描述">
              <Input placeholder="" />
            </Form.Item>
            <Form.Item
              name={'isCommentable'}
              valuePropName="checked"
              label="评论"
            >
              <Switch />
            </Form.Item>
            <div className={style.mask_oss}>
              <div className={style.mask_title}>封面</div>
              <div className={style.mask_ossimg}>
                <OssUpload changeImg={changeImg} />
                <Form.Item name="cover">
                  <Input placeholder="" />
                </Form.Item>
              </div>
            </div>
            <span>
              &emsp;&emsp;&emsp;
              <Button onClick={() => showDrawer()}>选择文件</Button>&emsp;
              <Button
                type="primary"
                danger
                ghost
                onClick={() =>
                  form.setFieldsValue({
                    cover: '',
                  })
                }
              >
                移除
              </Button>
            </span>
          </div>
          <div className={style.mask_off}>
            {type === 'update' ? (
              <Space>
                <Button onClick={onClose}>取消</Button>
                <Button type="primary" htmlType="submit">
                  更新
                </Button>
              </Space>
            ) : (
              <Space>
                <Button onClick={onClose}>取消</Button>
                <Button type="primary" htmlType="submit">
                  创建
                </Button>
              </Space>
            )}
          </div>
        </Form>
      </Drawer>
      <Aside {...{ open: open1, onClose: onClose1, onCheck }}></Aside>
    </div>
  );
};

export default Knowladge;
