import React from 'react';
import { InboxOutlined } from '@ant-design/icons';
import type { UploadProps } from 'antd';
import { message, Upload } from 'antd';
import style from './less/index.less';
const { Dragger } = Upload;

function OssUpload({changeImg} : {changeImg? :(img:string) => void}) {
  const props: UploadProps = {
    name: 'file',
    multiple: true,
    action: 'https://creationapi.shbwyz.com/api/file/upload?unique=0',
    onChange(info) {
      const { status } = info.file;
      if (status !== 'uploading') {
        
      }
      if (status === 'done') {
        console.log(info.file);
        changeImg && changeImg(info.file.response.data.url)
        
      } else if (status === 'error') {
        
      }
    },
    onDrop(e) {
      console.log('Dropped files', e.dataTransfer.files);
    },
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  };
  return (
    <div className={style.oss}>
      <Dragger {...props}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">点击选择文件或将文件拖拽到此处</p>
        <p className="ant-upload-hint">文件将上传到 OSS, 如未配置请先配置</p>
      </Dragger>
    </div>
  );
}

export default OssUpload;
