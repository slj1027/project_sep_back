import React, { FC, useState } from 'react';
import copy from 'copy-to-clipboard';
import { Card, Button, Drawer, message, Input, Popconfirm } from 'antd';
import style from '../../pages/file/less/index.less';
import style1 from './fileItem.less';
import moment from 'moment';
const { Meta } = Card;
function FileItem(props: any) {
  // console.log(props.item);
  const { item } = props;
  //  抽屉
  const [open, setOpen] = useState(false);

  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };
  //一键复制
  const handleClick = () => {
    copy(item.url);
    message.success('复制成功');
  };
  // 气泡弹出框
  const confirm = () => {
    message.info('没有后台接口');
  };
  return (
    <>
      <Card
        onClick={showDrawer}
        className={style.file_card}
        hoverable
        cover={
          <img className={style.file_card_img} alt="example" src={item.url} />
        }
      >
        {/* <div
            style={{
              backgroundImage: 'url(' + item.url + ')',
              backgroundSize: 'cover',
              backgroundPosition: '50%',
              color: '#999',
              fontSize: '30px',
              width: '100%',
              height: '160px',
            }}
          ></div> */}
        <Meta
          title={item.originalname}
          description={
            <p className={style['ant-card-meta-description']}>{`上传于${moment(
              item.createAt,
            ).format('YYYY-MM-DD HH:mm:ss')}`}</p>
          }
        />
      </Card>
      {/* 抽屉 */}
      <Drawer
        title="文件信息"
        placement="right"
        onClose={onClose}
        open={open}
        width={500}
      >
        <img src={item.url} alt="" style={{ width: '100%', height: '300px' }} />
        <p>文件名称：{item.originalname}</p>
        <p>存储路径：{item.filename}</p>
        <p>
          <span>文件类型：{item.type}</span>
          <span>文件大小：{(item.size / 1024).toFixed(2)}kb</span>
        </p>
        <p>
          访问链接：
          <Input placeholder={item.url} width="auto" />
        </p>
        <Button onClick={handleClick} style={{ color: 'blue' }}>
          复制
        </Button>
        <div className={style1.footer}>
          <Button onClick={onClose}>关闭</Button>
          <Popconfirm
            placement="topRight"
            title={'确认删除这个文件吗'}
            onConfirm={confirm}
            okText="Yes"
            cancelText="No"
          >
            <Button danger>删除</Button>
          </Popconfirm>
        </div>
      </Drawer>
    </>
  );
}

export default FileItem;
