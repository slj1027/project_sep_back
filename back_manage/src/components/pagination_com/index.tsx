/* 分页器 */
import { Pagination } from 'antd';
import style from './less/index.less';
interface PropsType {
  current: number;
  total: number;
  pageSize: number;
  onChange: (page: number, pageSize: number) => void;
}
const Pagination_com = (props: PropsType) => {
  const { current, total, onChange,pageSize } = props;
  return (
    <div className={style.page}>
      <Pagination
        className={style.pagination}
        showTotal={(total) => `共 ${total} 条`}
        current={current}
        total={total}
        pageSize={pageSize}
        pageSizeOptions={[8, 12, 16, 24, 36]}
        onChange={(page, pageSize) => onChange(page, pageSize)}
      />
    </div>
  );
};

export default Pagination_com;
