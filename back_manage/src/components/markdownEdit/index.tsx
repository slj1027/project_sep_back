/* 新建  &&  编辑 */
import React, { FC, useEffect, useRef, useState } from 'react';
import MDEditor from '@uiw/react-md-editor';
import { useParams } from 'umi';
import { throttle } from 'lodash';
import EditHeader from './components/editHeader';
import { getArticleItem, getPageItem } from '@/api';
import content1 from './markdown';
import style from './less/index.less';
import { _get_knowladgeItem } from '@/api/knowladge';
interface PropsType {
  url: string;
  item?: any;
  knowTitle?: string;
  knowArr?: any[];
  onChangeValue?: (params: any) => void;
}
const MarkdownEdit = ({
  url,
  knowTitle,
  knowArr,
  onChangeValue,
}: PropsType) => {
  let { id } = useParams() as { id: string };
  id = id ? id : '';
  const [item, setItem] = useState<any>({});
  let getItem = async () => {
    if (id) {
      if (url === 'article') {
        let data = await getArticleItem(id);
        setItem(data.data);
      } else if (url === 'page') {
        let data = await getPageItem(id);
        setItem(data.data);
      } else if (url === 'knowledge') {
        console.log(111, knowTitle);

        knowArr?.forEach((item: any) => {
          if (item.title === knowTitle) {
            console.log(item, '11111');

            setItem(item);
          }
        });
      }
    } else {
      setItem(undefined);
    }
  };
  useEffect(() => {
    getItem();
  }, [id, knowTitle]);
  const [value, setValue] = useState('');
  useEffect(() => {
    console.log(item);

    item ? setValue(() => item.content) : setValue(content1);
  }, [item]);
  const md = useRef<any>();
  const [toc, setToc] = useState<any>();
  const [html, setHtml] = useState<any>();
  useEffect(() => {
    setTimeout(() => {
      tocData();
    }, 0);
  }, [value, md]);
  useEffect(() => {
    setTimeout(() => {
      onChangeValue &&
        onChangeValue({
          content: value,
          html,
          toc,
        });
    }, 0);
  }, [toc]);
  //大纲
  const tocData = () => {
    let preview = md.current.querySelector('.w-md-editor-preview ');
    let previewHTML = md.current.querySelector('.wmde-markdown ');
    setHtml(previewHTML.innerHTML);
    let arr = Array.from(preview.querySelectorAll('*')).filter((item: any) =>
      /^H[1-6]$/.test(item.tagName),
    );
    let changeArr = arr.map((item: any, index: number) => {
      return {
        level: item.nodeName.slice(1),
        text: item.innerText,
        id: item.id,
      };
    });
    setToc(changeArr);
  };

  const [show, setShow] = useState(true);
  return (
    <div className={style.articleEdit}>
      {url !== 'knowledge' && (
        <EditHeader
          content={{ toc: JSON.stringify(toc), html, content: value }}
          url={url}
          item={item}
        />
      )}
      <main>
        <div className={style.mark}>
          <MDEditor
            style={{ width: '100%' }}
            height={'100vh'}
            value={value}
            onChange={throttle((value) => {
              setValue(value as string);
            }, 300)}
            ref={(val: any) => {
              if (val) {
                md.current = val.container;
              }
            }}
          />
        </div>
        {show ? (
          <div className={style.toc}>
            <h3>
              <span>大纲</span>
              <span className={style.close} onClick={() => setShow(false)}>
                x
              </span>
            </h3>

            <div>
              {toc &&
                toc.map((item: any) => {
                  return (
                    <div key={item.id} style={{ paddingLeft: item.level * 5 }}>
                      <a href={'#' + item.id}>{item.text}</a>
                    </div>
                  );
                })}
            </div>
          </div>
        ) : (
          <span className={style.close} onClick={() => setShow(true)}>
            大纲
          </span>
        )}
      </main>
    </div>
  );
};

export default MarkdownEdit;
