import React, { useEffect, useState } from 'react';
import {
  Dropdown,
  Menu,
  Space,
  message,
  Input,
  Button,
  Popconfirm,
  Modal,
} from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import Set from '../articleSet/set';
import style from './less/index.less';
import {
  addArticle,
  setArticleItem,
  setPageItem,
  addPage,
  delArticleItem,
} from '@/api';
import { history } from 'umi';
import PageSet from '../pageSet';
const { confirm } = Modal;
interface PropsType {
  content: any;
  item: any;
  url: string;
}
function EditHeader({ content, item, url }: PropsType) {
  const [title, setTitle] = useState('');
  useEffect(() => {
    if (url === 'article') {
      item && setTitle(item.title);
    } else {
      item && setTitle(item.name);
    }
  }, [item]);
  //设置-右侧展示
  const [open, setOpen] = useState(false);
  const showDrawer = () => {
    setOpen(true);
  };
  //文件预览
  const onClose = () => {
    setOpen(false);
  };

  const [data, setData] = useState<any>();
  const onFinish = (value: any) => {
    onClose();
    setData({
      ...value,
    });
  };
  const publish = () => {
    if (url === 'article') {
      let data1 = { ...data };
      data1.category =
        typeof data1.category === 'string'
          ? data1.category
          : data1.category && data1.category.id;
      data1.tags =
        typeof data1.tags === 'string'
          ? data1.tags
          : data1.tags && data1.tags.map((item: any) => item.id).join('');
      if (item) {
        setArticleItem(item.id, {
          ...item,
          ...data1,
          ...content,
          title,
          status: 'publish',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('页面更新成功');
            history.push('/work');
          }
        });
      } else {
        addArticle({
          ...data1,
          ...content,
          title,
          status: 'publish',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('发布成功');
            history.push('/work');
          }
        });
      }
    } else {
      if (item) {
        setPageItem(item.id, {
          ...item,
          ...data,
          ...content,
          name: title,
          status: 'publish',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('页面更新成功');
            history.goBack();
          }
        });
      } else {
        addPage({
          ...data,
          ...content,
          name: title,
          status: 'publish',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('发布成功');
            history.push('/work');
          }
        });
      }
    }
  };
  const draft = () => {
    if (url === 'article') {
      let data1 = { ...data };
      data1.category =
        typeof data1.category === 'string'
          ? data1.category
          : data1.category && data1.category.id;
      data1.tags =
        typeof data1.tags === 'string'
          ? data1.tags
          : data1.tags && data1.tags.map((item: any) => item.id).join('');
      if (item) {
        setArticleItem(item.id, {
          ...item,
          ...data1,
          ...content,
          title,
          status: 'draft',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('草稿保存成功');
            history.push('/work');
          }
        });
      } else {
        addArticle({
          ...data1,
          ...content,
          title,
          status: 'draft',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('草稿保存成功');
            history.push('/work');
          }
        });
      }
    } else {
      if (item) {
        setPageItem(item.id, {
          ...item,
          ...data,
          ...content,
          name: title,
          status: 'draft',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('页面更新成功');
            history.push('/work');
          }
        });
      } else {
        addPage({
          ...data,
          ...content,
          name: title,
          status: 'draft',
        }).then((res) => {
          if (res.statusCode >= 200 && res.statusCode < 300) {
            message.success('发布成功');
            history.push('/work');
          }
        });
      }
    }
  };
  const showConfirm = () => {
    confirm({
      title: '确认删除?',
      content: '删除内容后，无法恢复。',
      onOk() {
        delArticleItem(item.id);
      },
      onCancel() {
        console.log('Cancel');
      },
    });
  };
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <Button
              style={{
                width: '100%',
              }}
              disabled={item ? false : true}
            >
              查看
            </Button>
          ),
        },
        {
          key: '2',
          label: (
            <Button
              style={{
                width: '100%',
              }}
              onClick={showDrawer}
            >
              设置
            </Button>
          ),
        },
        {
          key: '3',
          label: (
            <Button
              style={{
                width: '100%',
              }}
              onClick={() => draft()}
            >
              保存草稿
            </Button>
          ),
        },
        {
          key: '4',
          label: (
            <Button
              style={{
                width: '100%',
              }}
              disabled={item ? false : true}
              onClick={showConfirm}
            >
              删除
            </Button>
          ),
        },
      ]}
    />
  );
  const confirmClose = () => {
    history.goBack();
  };
  return (
    <div className={style.editHeader}>
      {
        <div className={style.title}>
          <Popconfirm
            placement="topRight"
            title={'可能还有内容没有保存，确定关闭吗？'}
            onConfirm={confirmClose}
            okText="Yes"
            cancelText="No"
          >
            <div className={style.close}>x</div>
          </Popconfirm>

          <Input
            placeholder="请输入文章标题"
            value={title}
            onChange={(e) => {
              console.log(title);

              setTitle(e.target.value);
            }}
          />
          <Button type="primary" onClick={publish}>
            发表
          </Button>
          <Dropdown overlay={menu}>
            <a onClick={(e) => e.preventDefault()}>
              <Space>
                <EllipsisOutlined style={{ fontSize: '24px' }} />
              </Space>
            </a>
          </Dropdown>
        </div>
      }
      {url == 'article' ? (
        <Set {...{ open, onClose, onFinish, item }}></Set>
      ) : (
        <PageSet {...{ open, onClose, onFinish, item }} />
      )}
    </div>
  );
}

export default EditHeader;
