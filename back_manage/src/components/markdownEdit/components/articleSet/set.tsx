import React, { useEffect, useState } from 'react';
import { Button, Drawer, Form, Input, Switch, Select } from 'antd';
import useRequest from '@ahooksjs/use-request';
import { getCategory, getTags } from '@/api';
import Aside from '@/components/aside';
const { TextArea } = Input;
interface PropsType {
  open: boolean;
  onClose: () => void;
  onFinish: (value: any) => void;
  item: any;
}
function Set({ open, onClose, onFinish, item }: PropsType) {

  const [open1, setOpen1] = useState(false);
  const showDrawer1 = () => {
    setOpen1(true);
  };
  const onClose1 = () => {
    setOpen1(false);
  };
  const [form] = Form.useForm();
  const [img, setImg] = useState(() => '');
  const { data: tags } = useRequest(getTags);
  const { data: category } = useRequest(getCategory);
  const onCheck = (img: string) => {
    setImg(img);
  };
  useEffect(() => {
    if (item) {
      form.setFieldsValue({
        ...item,
      });
      setImg(item.cover);
    }
  }, [item]);
  return (
    <div>
      <Drawer
        title="文章设置"
        placement="right"
        onClose={onClose}
        open={open}
        size="large"
      >
        <Form
          onFinish={(value) =>
            onFinish({
              ...value,
              cover: img,
              
            })
          }
          form={form}
        >
          <Form.Item name={'summary'} label="文章摘要">
            <TextArea />
          </Form.Item>
          <Form.Item name={'password'} label="访问密码">
            <Input.Password />
          </Form.Item>
          <Form.Item name={'totalAmount'} label="付费查看">
            <Input.Password />
          </Form.Item>
          <Form.Item name={'isCommentable'} label="开启评论" valuePropName='checked'>
            <Switch />
          </Form.Item>
          <Form.Item name={'isRecommended'} label="首页推荐" valuePropName='checked'>
            <Switch />
          </Form.Item>
          <Form.Item name={'category'} label="选择分类">
            <Select>
              {category &&
                category.data &&
                category.data.map((item: any) => (
                  <Select.Option value={item.id} key={item.id}>
                    {item.label}
                  </Select.Option>
                ))}
            </Select>
          </Form.Item>
          <Form.Item name={'tags'} label="选择标签">
            <Select>
              {tags &&
                tags.data &&
                tags.data.map((item: any) => (
                  <Select.Option value={item.id} key={item.id}>
                    {item.label}
                  </Select.Option>
                ))}
            </Select>
          </Form.Item>
          <Form.Item name={'cover'} label="文章封面">
            <img
              src={img}
              alt="预览图"
              onClick={showDrawer1}
              width={300}
              height={150}
            />
            <Input placeholder="或输入外部链接" value={img} />
            <Button onClick={() => setImg('')}>移除</Button>
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit">确认</Button>
          </Form.Item>
        </Form>
      </Drawer>
      <Aside open={open1} onClose={onClose1} onCheck={onCheck} />
    </div>
  );
}

export default Set;
