import { useEffect, useState } from 'react';
import { Button, Drawer, Form, Input, InputNumber } from 'antd';
import { FileAddFilled } from '@ant-design/icons';
import Aside from '@/components/aside';
interface PropsType {
  open: boolean;
  onClose: () => void;
  onFinish: (value: any) => void;
  item: any;
}
function PageSet({ open, onClose, onFinish, item }: PropsType) {
  const [open1, setOpen1] = useState(false);
  const showDrawer1 = () => {
    setOpen1(true);
  };
  const onClose1 = () => {
    setOpen1(false);
  };
  const [form] = Form.useForm();
  const [img, setImg] = useState('');
  const onCheck = (img: string) => {
    setImg(img);
  };
  useEffect(() => {
    form.setFieldsValue({
      cover:img,
    });
  }, [img]);
  useEffect(() => {
    if (item) {
      form.setFieldsValue({
        ...item,
        cover:item.img
      });
      setImg(item.cover);
    }
  }, [item]);
  return (
    <div>
      <Drawer
        title="页面属性"
        placement="right"
        onClose={onClose}
        open={open}
        size="large"
      >
        <Form
          onFinish={(value) =>
            onFinish({
              ...value,
              cover: img,
            })
          }
          form={form}
        >
          <Form.Item name={'cover'} label="封面">
            <Input
              placeholder="或输入外部链接"
              addonAfter={<FileAddFilled onClick={showDrawer1} />}
              value={img}
            />
          </Form.Item>
          <Form.Item name={'path'} label="路径">
            <Input placeholder="或输入路径" />
          </Form.Item>
          <Form.Item name={'order'} label="顺序">
            <InputNumber min={0} defaultValue={0} />
          </Form.Item>
          <Form.Item>
            <Button htmlType="submit">确认</Button>
          </Form.Item>
        </Form>
      </Drawer>
      <Aside open={open1} onClose={onClose1} onCheck={onCheck} />
    </div>
  );
}

export default PageSet;
