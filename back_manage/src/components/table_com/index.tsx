/* 封装表格 */
import React, { FC } from 'react';
import { Table } from 'antd';
interface PropsType {
  dataSource: any;
  columns: any;
  rowSelection: any;
}
const Table_com = (props: PropsType) => {
  const { dataSource, columns, rowSelection } = props;
  return (
    <Table
      dataSource={dataSource}
      columns={columns}
      rowSelection={{
        type: 'checkbox',
        ...rowSelection,
      }}
      pagination={false}
      scroll={{ x: 1300 }}
      rowKey={(record) => record.id}
    />
  );
};

export default Table_com;
