import style from './less/index.less';
/* import { LoginForm, ProFormText } from '@ant-design/pro-components';
import { LockOutlined, UserOutlined } from '@ant-design/icons'; */
import { Button, Checkbox, Form, Input } from 'antd';
import { Col, Row } from 'antd';
import { useEffect } from 'react';
interface PropsType {
  title: string;
  img: any;
  finish: (value: any) => any;
}
const Login = (props: PropsType) => {
  const { title, finish, img } = props;
  useEffect(() => {
    localStorage.clear();
  });
  return (
    <div className={style.login}>
      <Row justify="space-between" className={style.antd_row}>
        <Col xs={0} sm={0} md={12}>
          <img src={img} alt="" width={'100%'} height={'100%'} />
        </Col>
        <Col xs={24} sm={24} md={12}>
          <div className={style.loginForm}>
            <h2>{title}</h2>
            <Form
              name="basic"
              initialValues={{ remember: true }}
              onFinish={finish}
              autoComplete="off"
              className={style.form}
              labelCol={{ span: 3 }}
            >
              <Form.Item
                label="用户名"
                name="name"
                rules={[
                  { required: true, message: 'Please input your username!' },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="密码"
                name="password"
                rules={[
                  { required: true, message: 'Please input your password!' },
                ]}
              >
                <Input.Password />
              </Form.Item>
              {title === '访客注册' && (
                <Form.Item
                  label="确认密码"
                  name="sure-password"
                  rules={[
                    { required: true, message: 'Please input your password!' },
                  ]}
                >
                  <Input.Password />
                </Form.Item>
              )}

              <Form.Item>
                <Button type="primary" htmlType="submit">
                  {title === '系统登录' ? '登录' : '注册'}
                </Button>
              </Form.Item>
            </Form>
            <div>
              {title === '系统登录' && <a href="/register">or 去注册</a>}
              {title === '访客注册' && <a href="/login">or 去登录</a>}
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default Login;
