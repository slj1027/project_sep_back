import React from 'react';
import style from '../less/index.less';
import { Popconfirm } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';
interface PropType {
  arr: any[];
  newArr: any[];
  active: string | undefined;
  changeActive: (item: any) => void;
  confirmDel: (id: string | number) => void;
}
function Nav({ arr, newArr, active, changeActive, confirmDel }: PropType) {
  const cancel = () => {};
  return (
    <ul>
      {arr &&
        arr.map((item: any, index: number) => {
          return (
            <li
              key={item.id}
              onClick={() => {
                changeActive(item);
              }}
              className={item.title === active ? style.active : style.no_active}
            >
              <div>{item.title}</div>
              <div>
                <Popconfirm
                  title="Are you sure to delete this task?"
                  onConfirm={() => confirmDel(item.id)}
                  onCancel={() => cancel()}
                  okText="Yes"
                  cancelText="No"
                >
                  <DeleteOutlined />
                </Popconfirm>
              </div>
            </li>
          );
        })}
      {newArr &&
        newArr.map((item: any, index: number) => {
          return (
            <li
              key={index}
              onClick={() => {
                changeActive(item);
              }}
              className={item.title === active ? style.active : style.no_active}
            >
              <div>{item.title}</div>
              <div>
                <Popconfirm
                  title="Are you sure to delete this task?"
                  onConfirm={() => confirmDel(index)}
                  onCancel={() => cancel()}
                  okText="Yes"
                  cancelText="No"
                >
                  <DeleteOutlined />
                </Popconfirm>
              </div>
            </li>
          );
        })}
    </ul>
  );
}

export default Nav;
