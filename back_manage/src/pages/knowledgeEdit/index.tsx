import React, { useState } from 'react';
import { Layout, Spin, Button, Popconfirm, Input } from 'antd';
import { SettingOutlined } from '@ant-design/icons';
import Knowladgemask from '@/components/knowladgemask';
import { useParams, history } from 'umi';
import { useRequest } from 'ahooks';
import {
  _edit_knowladge,
  _get_knowladgeItem,
  _status_knowladge,
  _del_knowladgeChapter,
} from '@/api/knowladge';
import style from './less/index.less';
import MarkdownEdit from '@/components/markdownEdit';
import Nav from './components/nav';
const { Sider, Content } = Layout;
function KnowledgeEdit() {
  const { id } = useParams() as { id: string };
  let { data, loading, run } = useRequest(() => _get_knowladgeItem(id));
  const [active, setActive] = useState<string | undefined>();
  const [arr, setArr] = useState<any[]>([]);
  const [open, setOpen] = useState(false);
  const changeActive = (item: any) => {
    setActive(item.title);
  };
  const [newTitle, setNewTitle] = useState('');
  const confirm = () => {
    let newArr = [...arr];
    newArr.push({
      title: newTitle,
      content: '',
      order: data.data.children.length + newArr.length,
      parentId: id,
      html: '',
      toc: '',
    });
    setArr(newArr);
    setActive(newTitle);
    setNewTitle('');
  };
  const onChangeValue = (params: any) => {
    let newArr = [...arr];
    newArr.forEach((item) => {
      if (item.title === active) {
        item.content = params.content;
        item.html = params.html;
        item.toc = JSON.stringify(params.toc);
      }
    });
    setArr(newArr);
    data.data.children.forEach((item: any) => {
      if (item.title === active) {
        item.content = params.content;
        item.html = params.html;
        item.toc = JSON.stringify(params.toc);
      }
    });
  };

  const save = () => {
    arr.forEach((item) => {
      _edit_knowladge([{ ...item }]);
    });
    data.data.children.forEach((item: any) => {
      _status_knowladge(item.id, {
        ...item,
      });
    });
    run();
    setArr([]);
  };

  const confirmDel = (id: string | number) => {
    if (typeof id === 'string') {
      _del_knowladgeChapter(id);
      run();
    } else {
      let newArr = [...arr];
      newArr.splice(id, 1);
      setArr(newArr);
    }
  };
  const confirmClose = () => {
    history.goBack();
  };
  return (
    <div className={style.knowEdit}>
      {loading ? (
        <Spin />
      ) : (
        <Layout>
          <Sider className={style.sider}>
            <header>
              <div className={style.head}>
                <Popconfirm
                  placement="topRight"
                  title={'可能还有内容没有保存，确定关闭吗？'}
                  onConfirm={confirmClose}
                  okText="Yes"
                  cancelText="No"
                >
                  <div className={style.close}>x</div>
                </Popconfirm>
                {data && data.data && (
                  <div className={style.title}>
                    <img src={data.data.cover} alt="" />
                    <span>{data.data.title}</span>
                  </div>
                )}
                <div>
                  <SettingOutlined onClick={() => setOpen(true)} />
                </div>
              </div>
              <div className={style.save}>
                <Button className={style.save} onClick={save}>
                  保存
                </Button>
              </div>
              <div>
                <span>
                  {data &&
                    data.data &&
                    data.data.children &&
                    data.data.children.length}
                  篇文章
                </span>
                <Popconfirm
                  placement="right"
                  title={
                    <Input
                      value={newTitle}
                      onChange={(e) => setNewTitle(e.target.value)}
                    />
                  }
                  onConfirm={confirm}
                  okText="新建"
                  showCancel={false}
                  icon={null}
                >
                  <Button>+新建</Button>
                </Popconfirm>
              </div>
            </header>
            <main>
              <Nav
                {...{
                  arr: data.data.children,
                  newArr: arr,
                  active,
                  changeActive,
                  confirmDel,
                }}
              ></Nav>
            </main>
          </Sider>
          <Content>
            <div className={style.content}>
              {active && (
                <MarkdownEdit
                  url={'knowledge'}
                  knowTitle={active}
                  knowArr={data.data.children.concat([...arr])}
                  onChangeValue={onChangeValue}
                />
              )}
            </div>
          </Content>
          <Knowladgemask
            open={open}
            onClose={() => {
              setOpen(false);
            }}
            type={'update'}
            id={id}
            obj={data.data}
            run={run}
          ></Knowladgemask>
        </Layout>
      )}
    </div>
  );
}

export default KnowledgeEdit;
