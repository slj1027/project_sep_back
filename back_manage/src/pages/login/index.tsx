/* 登录页 */
import { FC } from 'react';
import LoginForm from '@/components/login';
import { login } from '@/api';
import { loginData } from '@/types';
import { history } from 'umi';
const Login: FC = () => {
  const finish = (values:loginData) => {
    login(values).then(res=>{
      if(res.statusCode === 200){
        history.push('/work')
        localStorage.setItem('user',JSON.stringify(res.data));
        localStorage.setItem('token',JSON.stringify(res.data.token));
      }
    })
  };
  return (
    <LoginForm img={'http://mms2.baidu.com/it/u=3553263415,2059888521&fm=253&app=138&f=PNG&fmt=auto&q=75?w=610&h=500'} title={'系统登录'} finish={finish}></LoginForm>
  );
};

export default Login;
