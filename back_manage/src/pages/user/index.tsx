/* 用户管理 */
import { FC, useRef, useState } from 'react';
import { useRequest } from 'ahooks';
import { _getUserList, _disableButton, _shouButton } from '@/api/user';
import { ProTable, ProColumns, ActionType } from '@ant-design/pro-components';
import { Badge, Space, Breadcrumb } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import moment from 'moment';
import AffixHead from '@/components/affixHead';
import style from './less/index.less';

const User: FC = () => {
  // 接收批量数组
  const [ids, setID] = useState([{}]);
  const actionRef = useRef<ActionType | undefined>();
  const { runAsync } = useRequest(_getUserList, {
    manual: true,
  });

  const columns: ProColumns[] = [
    {
      title: '账号',
      dataIndex: 'name',
    },
    {
      title: '邮箱',
      dataIndex: 'email',
      width: 140,
    },
    {
      title: '角色',
      dataIndex: 'role',
      valueEnum:{
        admin: { text: '管理者' },
        visitor: { text: '访客' },
      },
      render: (_,record) => {
        // return 封装一个组件
        return record.role === 'admin' ? '管理者' : '访客';
      },
      width: 120,
    },
    {
      title: '状态',
      dataIndex: 'status',
      valueEnum:{
        locked: { text: '锁定' },
        active: { text: '可用' },
      },
      render: (_,record) => {
        return (record.status?.toString() as string) === 'active' ? (
          <Badge color={'green'} text={'可用'} />
        ) : (
          <Badge color={'yellow'} text={'已锁定'} />
        );
      },
    },
    {
      title: '注册日期',
      dataIndex: 'createAt',
      search: false,
      render: (item) => {
        return moment(item?.toString()).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '操作',
      search: false,
      render: (_, record) => {
        return (
          <div>
            <a
              onClick={async () => {
                record.status =
                  record.status === 'active' ? 'locked' : 'active';
                const res = await _disableButton(record);
                if (res.statusCode === 201) {
                  actionRef.current?.reload();
                }
              }}
            >
              {record.status === 'active' ? '禁用' : '启用'}
            </a>
            <a
              style={{ marginLeft: '10px' }}
              onClick={async () => {
                record.role = record.role === 'admin' ? 'visitor' : 'admin';
                const res = await _shouButton(record);
                if (res.statusCode === 201) {
                  actionRef.current?.reload();
                }
              }}
            >
              {record.role === 'admin' ? '解除授权' : '授权'}
            </a>
          </div>
        );
      },
    },
  ];
  //批量启用禁用
  const qisHandler = () => {
    ids.forEach((item: any) => {
      setTimeout(async () => {
        item.status = 'active';
        const res = await _shouButton(item);
        if (res.statusCode === 201) {
          actionRef.current?.reload();
        }
      }, 500);
    });
  };
  //批量禁用
  const jinHandler = () => {
    ids.forEach((item: any) => {
      setTimeout(async () => {
        item.status = 'locked';
        const res = await _shouButton(item);
        if (res.statusCode === 201) {
          actionRef.current?.reload();
        }
      }, 500);
    });
  };
  //批量授权
  const shouHandler = () => {
    ids.forEach((item: any) => {
      setTimeout(async () => {
        item.role = 'admin';
        const res = await _shouButton(item);
        if (res.statusCode === 201) {
          actionRef.current?.reload();
        }
      }, 500);
    });
  };
  //批量解除授权
  const jieHandler = () => {
    ids.forEach((item: any) => {
      setTimeout(async () => {
        item.role = 'visitor';
        const res = await _shouButton(item);
        if (res.statusCode === 201) {
          actionRef.current?.reload();
        }
      }, 500);
    });
  };
  return (
    <div className={style.user}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/user">用户管理</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <ProTable
          rowSelection={{
            type: 'checkbox',
            onChange: (_, records) => {
              setID(records);
            },
          }}
          actionRef={actionRef}
          columns={columns} // 列的配置
          rowKey={'id'}
          pagination={{
            showSizeChanger: true,
            // onChange: (page) => console.log(page),  //分页
          }}
          search={{
            searchText: '查询',
            span: 5,
            optionRender: (searchConfig, formProps, dom) => {
              return dom;
            },
          }}
          tableAlertRender={() => {
            return (
              <Space size={16}>
                <a onClick={qisHandler}>启用</a>
                <a onClick={jinHandler}>禁用</a>
                <a onClick={jieHandler}>解除授权</a>
                <a onClick={shouHandler}>授权</a>
              </Space>
            );
          }}
          toolbar={{
            settings: [
              <ReloadOutlined
                onClick={() => actionRef.current?.reload()}
              ></ReloadOutlined>,
            ],
          }}
          request={async (options: any) => {
            // 初始化的创建表格执行， 分页 改变执行， 点击查询的时候 也会执行
            options = {
              ...options,
              page: options.current,
            };
            delete options.current;
            const { data } = await runAsync(options); // 手动发起请求
            //
            return {
              options,
              data: data[0],
              success: true,
              total: data[1],
            };
          }}
        ></ProTable>
      </main>
    </div>
  );
};
export default User;
