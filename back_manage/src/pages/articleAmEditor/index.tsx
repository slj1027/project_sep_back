/* 新建——文章富文本编辑 */
import { FC, useState } from 'react';
import { history } from 'umi';
import {
  Dropdown,
  Menu,
  Space,
  Input,
  Button,
  Popconfirm,
} from 'antd';
import { EllipsisOutlined } from '@ant-design/icons';
import style from './less/index.less'
const ArticleAmEditor: FC = () => {
  const [title, setTitle] = useState('');
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: <Button disabled>查看</Button>,
        },
        {
          key: '2',
          label: <Button>设置</Button>,
        },
        {
          key: '3',
          label: <Button>保存草稿</Button>,
        },
        {
          key: '4',
          label: (
            <Button disabled>
              删除
            </Button>
          ),
        },
      ]}
    />
  );
  const confirmClose = () => {
    history.goBack();
  };
  return (
    <div className={style.amEditor}>
      <header>
        <div className={style.editHeader}>
          {
            <div className={style.title}>
              <Popconfirm
                placement="topRight"
                title={'可能还有内容没有保存，确定关闭吗？'}
                onConfirm={confirmClose}
                okText="Yes"
                cancelText="No"
              >
                <div className={style.close}>x</div>
              </Popconfirm>

              <Input
                placeholder="请输入文章标题"
                value={title}
                onChange={(e) => {
                  console.log(title);

                  setTitle(e.target.value);
                }}
              />
              <Button type="primary">发表</Button>
              <Dropdown overlay={menu}>
                <a onClick={(e) => e.preventDefault()}>
                  <Space>
                    <EllipsisOutlined style={{ fontSize: '24px' }} />
                  </Space>
                </a>
              </Dropdown>
            </div>
          }
        </div>
        {/* <EditHeader /> */}
      </header>
      <main>
        <iframe src="https://jasonandjay.com/editor/static/" width={'100%'} height={'100vh'}></iframe>
      </main>
    </div>
  );
};

export default ArticleAmEditor;
