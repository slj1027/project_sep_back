import { get_tag } from '../../../api/index';
const classify = {
  namespace: 'classify',
  state: {
    tagList: [],
  },
  reducers: {
    get_tag(state: any, { payload }: any) {
      return {
        ...state,
        tagList: [...payload],
      };
    },
  },
  effects: {
    *getTag(_: any, { put, call }: any) {
      const data: { data: any[] } = yield call(get_tag);
      yield put({
        type: 'get_tag',
        payload: [...data.data],
      });
    },
  },
};
export default classify;
