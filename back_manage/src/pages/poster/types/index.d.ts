export interface ossPosterData {
  unique:number;
}
export interface searchPosterData {
  page:number;
  pageSize:number;
  name:string;
}