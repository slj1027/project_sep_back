/* 海报管理 */

import { getPoster } from '@/api';
import { useRequest } from 'ahooks';
import React, { FC, useState } from 'react';
import Table_com from '@/components/table_com';
import Pagination_com from '@/components/pagination_com';
import OssUpload from '@/components/ossUpload';
import SearchBox from './components/searchBox';
import { Space, Spin, Empty, Breadcrumb } from 'antd';
import style from './less/index.less';
import AffixHead from '@/components/affixHead';
const Poster: FC = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { loading, data, run } = useRequest(() =>
    getPoster({
      page,
      pageSize,
    }),
  );

  //*渲染
  const columns = [
    {
      title: '创建时间',
      dataIndex: 'createAt',
      key: 'createAt',
      width: 260,
    },
    {
      title: 'Action',
      key: 'action',
      fixed: 'right',
      width: 240,
      render: (_: any, record: any) => (
        <Space size="middle">
          <span
            onClick={() => {
              console.log(record);
            }}
          >
            通过
          </span>
          <span onClick={() => {}}>拒绝</span>
          <span onClick={() => {}}>回复</span>
          {/* <Popconfirm
            title="Are you sure to delete this task?"
            onConfirm={confirm}
            onCancel={cancel}
            okText="Yes"
            cancelText="No"
          >
            <a href="#" onClick={() => {}}>
              删除
            </a>
          </Popconfirm> */}
        </Space>
      ),
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any) => {
      console.log(
        `selectedRowKeys: ${selectedRowKeys}`,
        'selectedRows: ',
        selectedRows,
      );
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === 'Disabled User', //* Column configuration not to be checked
      name: record.name,
    }),
  };
  const onChange = (page: number, pageSize: number) => {
    setPage(page), setPageSize(pageSize);
  };

  const search = () => {
    setPage(1);
  };
  return (
    <div className={style.poster}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/poster">海报管理</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <OssUpload></OssUpload>
        <SearchBox {...{ page, pageSize, search }}></SearchBox>
        <div>
          {/* <Table_com
            {...{ dataSource: data?.data[0], columns, rowSelection }}
          ></Table_com>
          <Pagination_com
            {...{ current: page, total: data?.data[1], onChange }}
          ></Pagination_com> */}
          {data && data.data && data.data[1] ? <div></div> : <Empty />}
          {loading && <Spin />}
        </div>
      </main>
    </div>
  );
};
export default Poster;
