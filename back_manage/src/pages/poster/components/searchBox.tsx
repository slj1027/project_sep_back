import { useState } from 'react';
import { Input, Button } from 'antd';
import style from '../less/index.less';
import { useRequest } from 'ahooks';
import { searchPoster } from '@/api';
interface PropsType {
  page: number;
  pageSize: number;
  search:()=>void;
}
function SearchBox({ page, pageSize }:PropsType) {
  const [value, setValue] = useState('');
  const search = () => {};
  const {} = useRequest(() =>
    searchPoster({
      page,
      pageSize,
      name: value,
    }),
  );
  return (
    <div className={style.search}>
      <div className={style.searchInp}>
        <span>文件名称：</span>
        <Input
          type="text"
          placeholder="请输入称呼"
          onChange={(e) => {
            setValue(e.target.value);
          }}
          value={value}
        />
      </div>
      <div className={style.searchBut}>
        <Button onClick={() => setValue('')}>重置</Button>
        <Button type="primary" onClick={() => search()}>
          搜索
        </Button>
      </div>
    </div>
  );
}

export default SearchBox;
