import {request} from 'umi'
export const getRemoteList=async({page,pageSize,originalname,type}:{
  page:number,
  pageSize:number,
  originalname:string,
  type:string
})=>{
   return  request(`/api/api/file?page=${page}&pageSize=${pageSize}&originalname=${originalname}&type=${type}`, {
    method: 'get',
  })
    .then(function(response) {
      return response.data
    })
    .catch(function(error) {
      console.log(error);
      return false;
    });
}
//搜索
export const getSearchList=async({page,pageSize,originalname,type}:{
  page:number,
  pageSize:number,
  originalname:string,
  type:string
})=>{
   return  request(`/api/api/file?page=${page}&pageSize=${pageSize}&originalname=${originalname}&type=${type}`, {
    method: 'get',
  })
    .then(function(response) {
      return response.data
    })
    .catch(function(error) {
      console.log(error);
      return false;
    });
}