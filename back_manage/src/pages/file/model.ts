import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {getRemoteList,getSearchList} from './service'
interface PageModelType{
  namespace:'files',
  state:{},
  reducers:{
    getList:Reducer;
  },
  effects:{
    getRemote:Effect;
  },
  subscriptions:{
    setup:Subscription;
  }

}


const PageModel:PageModelType = {
  //命名空间
  namespace: 'files',
  state: {},
  reducers: {
    getList(state,{payload}){
      return payload;
    }
  },
  effects: {
    *getRemote({payload:{page,pageSize,originalname,type}},{put,call}):any{
      const data=yield call(getRemoteList,{page,pageSize,originalname,type});
      console.log(data[0]);
      if(data[0]){
        yield put({
          type:'getList',
          payload:data
        });
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
       return history.listen(({pathname}:any) => {
         if(pathname==='/file'){
            dispatch({
               type:'getRemote',
               payload:{
                 page:1,
                 pageSize:12,
                 originalname:'',
                 type:''
               }
            })
         }
      });
    },
  },
};

export default PageModel;
