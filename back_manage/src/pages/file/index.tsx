/* 文件管理 */
import React, { FC, useState } from 'react';
import { connect } from 'dva';
import { Col, Row, Pagination, Breadcrumb } from 'antd';
import AffixHead from '@/components/affixHead';
import style from './less/index.less';
interface valuesType {
  name?: string;
  type?: string;
}
import { ProFormText, QueryFilter } from '@ant-design/pro-components';

// 引入组件
import FileItem from '../../components/fileItem';
import OssUpload from '@/components/ossUpload';
const File: FC = ({ files, dispatch }: any) => {
  // 搜索内容放到全局
  const [types, setTypes] = useState('');
  const [origina, setOrigina] = useState('');
  // 搜索
  const searchHander = async (values: valuesType) => {
    //  console.log(values);
    if (values.type !== undefined) {
      setTypes(values.type);
    }
    if (values.name !== undefined) {
      setOrigina(values.name);
    }
    dispatch({
      type: 'files/getRemote',
      payload: {
        page: 1,
        pageSize: 12,
        originalname: values.name ? values.name : '',
        type: values.type ? values.type : '',
      },
    });
  };
  //点击分页
  const paginationHanderle = (page: number, pageSize: number) => {
    //  console.log(page,pageSize);
    dispatch({
      type: 'files/getRemote',
      payload: {
        page,
        pageSize,
        originalname: origina,
        type: types,
      },
    });
  };
  //改变每个条数
  const pageSizeHanderle = (current: number, size: number) => {
    console.log(current, size);
    dispatch({
      type: 'files/getRemote',
      payload: {
        page: current,
        pageSize: size,
        originalname: origina,
        type: types,
      },
    });
  };
  return (
    <div className={style.file_wrap}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/file">文件管理</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      {/* 搜索 */}
      <main>
        <OssUpload />
        <div className={style.file_search}>
          <QueryFilter<{
            name: string;
            company: string;
          }>
            onFinish={searchHander}
          >
            <ProFormText name="name" label="文件名称" />
            <ProFormText name="type" label="文件类型" />
          </QueryFilter>
        </div>
        <div className={style.file_img}>
          {/* 图片渲染 */}
          <Row gutter={24}>
            {files[0] &&
              files[0].map((item: any, index: number) => {
                return (
                  <Col
                    key={index}
                    xs={{ span: 24 }}
                    sm={{ span: 10 }}
                    md={{ span: 6 }}
                    lg={{ span: 5 }}
                    xl={{ span: 4 }}
                  >
                    <FileItem key={index} item={item} />
                  </Col>
                );
              })}
          </Row>
          {/* 分页 */}
          <div className={style.pagination}>
            <Pagination
              total={files[1]}
              showTotal={() => `共 ${files[1]} 条`}
              defaultPageSize={12}
              defaultCurrent={1}
              pageSizeOptions={[12, 24, 36]}
              showSizeChanger
              onChange={paginationHanderle}
              onShowSizeChange={pageSizeHanderle}
            />
          </div>
        </div>
      </main>
    </div>
  );
};

const mapStateToProps = ({ files }: any) => {
  return {
    files,
  };
};
export default connect(mapStateToProps)(File);
