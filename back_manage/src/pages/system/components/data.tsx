import React, { useEffect } from 'react';
import { Button, Form, Input } from 'antd';
import { setBase } from '@/api';
interface PropsType {
  baiduAnalyticsId: string;
  googleAnalyticsId: string;
}
function Data({ baiduAnalyticsId, googleAnalyticsId }: PropsType) {
  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue({
      baiduAnalyticsId,
      googleAnalyticsId,
    });
  }, []);
  const onFinish = (value: any) => {
    setBase({...value}).then(res=>{
      form.setFieldsValue({...res.data})
    })
  };
  return (
    <div>
      <Form
        layout={'vertical'}
        form={form}
        initialValues={{ layout: 'vertical' }}
        onFinish={onFinish}
      >
        <Form.Item label="百度分析" name={'baiduAnalyticsId'}>
          <Input />
        </Form.Item>
        <Form.Item label="谷歌分析" name={'googleAnalyticsId'}>
          <Input />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">保存</Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Data;
