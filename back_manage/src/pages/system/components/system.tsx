import { setBase } from '@/api';
import { Button, Form, Input } from 'antd';
import { FileAddFilled } from '@ant-design/icons';
import { useEffect, useState } from 'react';
import Aside from '@/components/aside';
interface PropsType {
  systemUrl: string;
  adminSystemUrl: string;
  systemTitle: string;
  systemLogo: string;
  systemFavicon: string;
  systemFooterInfo: string;
}
const SystemSetting = ({
  systemUrl,
  adminSystemUrl,
  systemTitle,
  systemLogo,
  systemFavicon,
  systemFooterInfo,
}: PropsType) => {
  const [form] = Form.useForm();
  const [type, setType] = useState('');
  useEffect(() => {
    form.setFieldsValue({
      systemUrl,
      adminSystemUrl,
      systemTitle,
      systemLogo,
      systemFavicon,
      systemFooterInfo,
    });
  }, []);
  const onFinish = (value: any) => {
    setBase({ ...value }).then((res) => {
      form.setFieldsValue({ ...res.data });
    });
  };
  const [open, setOpen] = useState(false);
  const onCheck = (img: string) => {
    if (type === 'logo') {
      form.setFieldsValue({ systemLogo: img });
    } else {
      form.setFieldsValue({ systemFavicon: img });
    }
  };
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  return (
    <div>
      <Form
        layout={'vertical'}
        form={form}
        initialValues={{ layout: 'vertical' }}
        onFinish={onFinish}
      >
        <Form.Item label="系统地址" name={'systemUrl'}>
          <Input />
        </Form.Item>
        <Form.Item label="后台地址" name={'adminSystemUrl'}>
          <Input />
        </Form.Item>
        <Form.Item label="系统标题" name={'systemTitle'}>
          <Input />
        </Form.Item>
        <Form.Item label="LOGO" name="systemLogo">
          <Input
            addonAfter={
              <FileAddFilled
                onClick={() => {
                  setType('logo');
                  showDrawer();
                }}
              />
            }
          />
        </Form.Item>
        <Form.Item label="Favicon" name={'systemFavicon'}>
          <Input
            addonAfter={
              <FileAddFilled
                onClick={() => {
                  setType('icon');
                  showDrawer();
                }}
              />
            }
          />
        </Form.Item>
        <Form.Item label="页脚信息" name={'systemFooterInfo'}>
          <Input />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            保存
          </Button>
        </Form.Item>
      </Form>
      <Aside {...{ open, onClose, onCheck }}></Aside>
    </div>
  );
};

export default SystemSetting;
