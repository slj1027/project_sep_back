import { useState } from 'react';
import { Tabs, Button } from 'antd';
import MonacoEdit from '@/components/manacoEdit';
import { setBase } from '@/api';

function International({ i18n }: { i18n: string }) {
  let data = JSON.parse(i18n);
  const changeValue = (value: string, code: string) => {
    console.log(activeKey);
    data[activeKey] = JSON.stringify(value);
  };
  const initialItems = Object.keys(data).map((item, index) => {
    return {
      label: item,
      children: (
        <MonacoEdit code={data[item]} changeValue={changeValue}></MonacoEdit>
      ),
      key: item,
    };
  });
  const [activeKey, setActiveKey] = useState(
    initialItems[0] && initialItems[0].key,
  );
  const [items, setItems] = useState(initialItems);

  const onChange = (newActiveKey: string) => {
    setActiveKey(newActiveKey);
  };

  const save = () => {
    setBase(JSON.stringify(data));
  };
  return (
    <div>
      <Tabs
        type="editable-card"
        onChange={onChange}
        activeKey={activeKey}
        items={items}
      />
      <Button type="primary" onClick={save}>
        保存
      </Button>
    </div>
  );
}

export default International;
