import { useEffect } from 'react';
import { Button, Form, Input } from 'antd';
import { setBase, testSendEmail } from '@/api';
interface PropsType {
  smtpHost: string;
  smtpPort: string;
  smtpUser: string;
  smtpPass: string;
  smtpFromUser: string;
}
function Smtp({
  smtpHost,
  smtpPort,
  smtpUser,
  smtpPass,
  smtpFromUser,
}: PropsType) {
  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue({
      smtpHost,
      smtpPort,
      smtpUser,
      smtpPass,
      smtpFromUser,
    });
  }, []);
  const onFinish = (value: any) => {
    setBase({ ...value }).then((res) => {
      form.setFieldsValue({ ...res.data });
    });
  };
  return (
    <div>
      <Form
        layout={'vertical'}
        form={form}
        initialValues={{ layout: 'vertical' }}
        onFinish={onFinish}
      >
        <Form.Item label="SMTP地址" name={'smtpHost'}>
          <Input />
        </Form.Item>
        <Form.Item label="SMTP端口（强制使用SSL链接" name={'smtpPort'}>
          <Input />
        </Form.Item>
        <Form.Item label="SMTP用户" name={'smtpUser'}>
          <Input />
        </Form.Item>
        <Form.Item label="SMTP密码" name={'smtpPass'}>
          <Input />
        </Form.Item>
        <Form.Item label="发件人" name={'smtpFromUser'}>
          <Input />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            保存
          </Button>
          <Button
            onClick={() => {
              testSendEmail({
                subject: '测试邮件',
                text: '测试邮件',
                to: form.getFieldsValue().smtpFromUser,
              });
            }}
          >
            测试
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Smtp;
