import { useEffect } from 'react';
import { Button, Form, Input, Radio } from 'antd';
import { setBase } from '@/api';
interface PropsType {
  seoKeyword: string;
  seoDesc: string;
}
function Seo({ seoKeyword, seoDesc }: PropsType) {
  const [form] = Form.useForm();
  useEffect(() => {
    form.setFieldsValue({
      seoKeyword, seoDesc
    });
  }, []);
  const onFinish = (value: any) => {
    setBase({...value}).then(res=>{
      form.setFieldsValue({...res.data})
    })
  };
  return (
    <div>
      <Form
        layout={'vertical'}
        form={form}
        initialValues={{ layout: 'vertical' }}
        onFinish={onFinish}
      >
        <Form.Item label="关键词" name={'seoKeyword'}>
          <Input />
        </Form.Item>
        <Form.Item label="描述信息" name={'seoDesc'}>
          <Input />
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">保存</Button>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Seo;
