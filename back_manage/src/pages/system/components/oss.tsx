import React, { useEffect, useState } from 'react';
import { Alert, Button } from 'antd';
import ManacoEdit from '@/components/manacoEdit';
import { setBase } from '@/api';
function Oss({ oss }: { oss: any }) {
  const [value, setValue] = useState('');
  useEffect(() => {
    setValue(oss);
  }, []);
  const changeValue = (value: string) => {
    console.log(value);
    setValue(value);
  };
  const save = () => {
    setBase({ oss: value }).then((res) => {
      setValue(res.data.oss);
    });
  };
  return (
    <div>
      <div>
        <Alert
          message="说明"
          description={
            <div>
              <p>请在编辑器中输入您的 oss 配置，并添加 type 字段区分</p>
              <p>
                {
                  '{"type":"aliyun","accessKeyId":"","accessKeySecret":"","bucket":"","https":true,"region":""}'
                }
              </p>
            </div>
          }
          type="info"
          showIcon
        />
      </div>
      <ManacoEdit code={value} changeValue={changeValue} />
      <Button onClick={save}>保存</Button>
    </div>
  );
}

export default Oss;
