/* 系统设置 */

import { Tabs, Breadcrumb } from 'antd';
import React, { FC } from 'react';
import { useRequest } from 'ahooks';
import SystemSetting from './components/system';
import International from './components/international';
import Seo from './components/seo';
import Data from './components/data';
import Oss from './components/oss';
import Smtp from './components/smtp';
import { getView } from '@/api';
import style from './less/index.less';
import AffixHead from '@/components/affixHead';
const System: FC = () => {
  const { data } = useRequest(getView);
  const items = [
    {
      label: `系统设置`,
      key: '1',
      children: data && <SystemSetting {...data.data} />,
    },
    {
      label: `国际化设置`,
      key: '2',
      children: data && <International {...data.data} />,
    },
    {
      label: `SEO设置`,
      key: '3',
      children: data && <Seo {...data.data} />,
    },
    {
      label: `数据统计`,
      key: '4',
      children: data && <Data {...data.data} />,
    },
    {
      label: `OSS设置`,
      key: '5',
      children: data && <Oss {...data.data} />,
    },

    {
      label: `SMTP服务`,
      key: '6',
      children: data && <Smtp {...data.data} />,
    },
  ];
  return (
    <div className={style.system}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/system">系统设置</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <div>
          <Tabs
            tabPosition={'left'}
            items={items}
            className={style['ant-tabs']}
          />
        </div>
      </main>
    </div>
  );
};
export default System;
