/* 注册 */

import React, { FC } from 'react';
import LoginForm from '@/components/login';
import { loginData } from '@/types';
import { register } from '@/api';
import { history } from 'umi';
const Register: FC = () => {
  const finish = (values: loginData) => {
    register(values).then((res) => {
      if (res.statusCode >= 200 && res.statusCode < 300) {
        history.push('/work');
        localStorage.setItem('user', JSON.stringify(res.data));
        localStorage.setItem('token', JSON.stringify(res.data.token));
      }
    });
  };
  return (
    <LoginForm
      img={
        'http://mms0.baidu.com/it/u=2439825956,785531126&fm=253&app=138&f=PNG&fmt=auto&q=75?w=702&h=500'
      }
      title={'访客注册'}
      finish={finish}
    ></LoginForm>
  );
};
export default Register;
