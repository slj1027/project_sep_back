import { Effect, ImmerReducer, Reducer, Subscription } from 'umi';
import {
  getRemoteList,
  deleteList,
  editorList,
  publishList,
  draftList,
  draftsList,
} from './service';
import { message } from 'antd';
interface PageModelType {
  namespace: 'pages';
  state: {};
  reducers: {
    getList: Reducer;
    geteditorList: Reducer;
    getpublishList: Reducer;
    getDraftList: Reducer;
    getDraftsList: Reducer;
  };
  effects: {
    getRemote: Effect;
    delete: Effect;
    editor: Effect;
    publish: Effect;
    draft: Effect;
    drafts: Effect;
  };
  subscriptions: {
    setup: Subscription;
  };
}

const PageModel: PageModelType = {
  //命名空间
  namespace: 'pages',
  state: {},
  reducers: {
    getList(state, { payload }) {
      return payload;
    },
    geteditorList(state, { payload }) {
      return payload;
    },
    getpublishList(state, { payload }) {
      return payload;
    },
    getDraftList(state, { payload }) {
      return payload;
    },
    getDraftsList(state, { payload }) {
      return payload;
    },
  },
  effects: {
    *getRemote(
      { payload: { page, pageSize, name, path, status } },
      { put, call },
    ): any {
      const data = yield call(getRemoteList, {
        page,
        pageSize,
        name,
        path,
        status,
      });
      console.log(data);
      if (data) {
        yield put({
          type: 'getList',
          payload: data,
        });
      }
    },
    *delete({ payload: { id } }, { put, call, select }): any {
      const data = yield call(deleteList, { id });
      if (data) {
        message.success('Delete successfully.');
        const { page, pageSize } = yield select((state: any) => state);

        yield put({
          type: 'getRemote',
          payload: {
            page: 1,
            pageSize: 10,
            name: '',
            path: '',
            status: '',
          },
        });
      } else {
        message.error('Delete failed.');
      }
    },
    *editor({ payload: { id } }, { put, call }): any {
      const data = yield call(editorList, { id });
      if (data) {
        // message.success('修改成功');
        yield put({
          type: 'geteditorList',
          payload: data,
        });
      } else {
        message.error('编辑失败');
      }
    },
    *publish({ payload: { id, datas } }, { put, call }): any {
      const data = yield call(publishList, { id, datas });
      if (data) {
        message.success('发布修改成功');
        yield put({
          type: 'getpublishList',
          payload: data,
        }),
          yield put({
            type: 'getRemote',
            payload: {
              page: 1,
              pageSize: 10,
              name: '',
              path: '',
              status: '',
            },
          });
      } else {
        message.error('发布失败');
      }
    },
    *draft({ payload: { id, datas } }, { put, call }): any {
      const data = yield call(draftList, { id, datas });
      if (data) {
        message.success('操作成功');
        yield put({
          type: 'getDraftList',
          payload: data,
        }),
          yield put({
            type: 'getRemote',
            payload: {
              page: 1,
              pageSize: 10,
              name: '',
              path: '',
              status: '',
            },
          });
      } else {
        message.error('发布失败');
      }
    },
    *drafts({ payload: { id, datas } }, { put, call }): any {
      const data = yield call(draftList, { id, datas });
      if (data) {
        message.success('操作成功');
        yield put({
          type: 'getDraftsList',
          payload: data,
        });
        yield put({
          type: 'getRemote',
          payload: {
            page: 1,
            pageSize: 10,
            name: '',
            path: '',
            status: '',
          },
        });
      } else {
        message.error('发布失败');
      }
    },
  },

  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (location.pathname === '/page') {
          dispatch({
            type: 'getRemote',
            payload: {
              page: 1,
              pageSize: 10,
              name: '',
              path: '',
              status: '',
            },
          });
        }
      });
    },
  },
};

export default PageModel;
