import { request } from 'umi';
export const getRemoteList = async (params: {
  page: number;
  pageSize: number;
  name: string;
  path: string;
  status: string;
}) => {
  return request(`/api/api/page`, {
    method: 'get',
    params,
  })
    .then(function (response) {
      return response.data;
    })
    .catch(function (error) {
      console.log(error);
    });
};
//删除
export const deleteList = async ({ id }: { id: string }) => {
  return request(`/api/api/page/${id}`, {
    method: 'delete',
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  })
    .then(function (response) {
      return true;
    })
    .catch(function (error) {
      return false;
      console.log(error);
    });
};
//编辑
export const editorList = async ({ id }: { id: string }) => {
  return request(`/api/api/page/${id}`, {
    method: 'get',
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  })
    .then(function (response) {
      console.log(response.data, '999');

      return response.data;
    })
    .catch(function (error) {
      return false;
      console.log(error);
    });
};
//点击发布
export const publishList = async ({
  id,
  datas,
}: {
  id: string;
  datas: any;
}) => {
  return request(`/api/api/page/${id}`, {
    method: 'PATCH',
    data: datas,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  })
    .then(function (response) {
      console.log(response.data, '999');

      return response.data;
    })
    .catch(function (error) {
      return false;
      console.log(error);
    });
};
//点击发布下线
export const draftList = async ({
  id,
  datas,
}: {
  id: string;
  datas: string;
}) => {
  console.log(datas, '222');

  return request(`/api/api/page/${id}`, {
    method: 'PATCH',
    data: datas,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  })
    .then(function (response) {
      console.log(response.data, '999');

      return response.data;
    })
    .catch(function (error) {
      return false;
      console.log(error);
    });
};
//批量发布 下线
export const draftsList = async ({
  id,
  datas,
}: {
  id: string;
  datas: string;
}) => {
  console.log(datas, '666');

  return request(`/api/api/page/${id}`, {
    method: 'PATCH',
    data: datas,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  })
    .then(function (response) {
      console.log(response.data, '999');

      return response.data;
    })
    .catch(function (error) {
      return false;
      console.log(error);
    });
};
