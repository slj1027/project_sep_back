



import React, { FC, useEffect, useState } from 'react'
import style from './editor.less'
import './editor.less'
import { connect } from 'dva';
import { history, useLocation } from 'umi'
import { Input, Form, Popconfirm, Button, message, Dropdown, Menu, Space, Drawer,InputNumber} from 'antd'
import MDEditor from '@uiw/react-md-editor';

interface paramsType {
  id: string
}

const Edit: FC = ({ pages, dispatch }: any) => {

  const location: any = useLocation()
  // console.log(location.state.state.name, '000');

  const [value, setValue] = useState(() => location.state.state && location.state.state.content)

  // from表单储存
  const [notes,setNotes]=useState(()=>location.state.state && location.state.state)
  console.log(notes,'notes');
  
  //抽屉
  const [open, setOpen] = useState(false);
  const [form] = Form.useForm();
  form.setFieldsValue({ note: notes.name, path:notes.path,views:notes.views})

  // 抽屉
  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };

  const confirm = () => {
    message.info('Clicked on Yes.');
    history.push(`/page`)
    dispatch({
      type: "pages/getRemote",
      payload: {
        page: 1,
        pageSize: 3,
        name: '',
        path: '',
        status: '',
      }
    })
  };
  // 下拉菜单
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <a target="_blank" rel="noopener noreferrer" href="https://www.antgroup.com">
              查看
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a target="_blank" rel="noopener noreferrer" onClick={showDrawer}>
              设置
            </a>
          ),
        },
        {
          key: '3',
          label: (
            <a target="_blank" rel="noopener noreferrer" onClick={()=>{
              setNotes({
                ...notes,
                status:"draft"
              })
              console.log(notes,'----');
              // dispatch({
              //     type: "pages/publish",
              //     payload: {
              //       id: location.state.state.id,
              //       datas:notes
              //     }
              //   })
            }}>
              保存草稿
            </a>
          ),
        },
        {
          key: '4',
          label: (
            <a target="_blank" rel="noopener noreferrer" onClick={()=>{
              dispatch({
                type: 'pages/delete',
                payload: {
                  id:notes.id,
                },
              });
              history.push(`/page`)
            }}>
              删除
            </a>
          ),
        },
      ]}
    />
  );
  // 点击发布
  const publishHandler = () => {
    
    dispatch({
      type: "pages/publish",
      payload: {
        id: location.state.state.id,
        datas:notes
      }
    })
  }
  //提交
  // const onFinish = (values: any) => {
  //   // dispatch({
  //   //   type: "pages/publish",
  //   //   payload: {
  //   //     id: location.state.state.id,
  //   //     datas: { name: values.note }
  //   //   }
  //   // })
  //   history.push(`/page`)
  //   // console.log('Success:', values.note);
  // };
  return (
    <div className={style.editor_wrap}>
      <div className={style.editor_top}>
        {/* 确认框 */}
        <Form className={style.form} form={form} name="control-hooks"
        >
          <div className={style.editor_top_left}>
            <Popconfirm
              placement="rightTop"
              title={'确认关闭吗？如果有内容变更，请先保存'}
              onConfirm={confirm}
              okText="确认"
              cancelText="取消"
            >
              <Button>x</Button>
            </Popconfirm>
            <Form.Item name="note">
              <Input bordered={false} value={notes.name} className={style.input} onChange={(e)=>{
                // console.log(e.target.value);
                // notes.name=e.target.value
                setNotes({
                  ...notes,
                  name:e.target.value
                })
              }} />
            </Form.Item>
          </div>
          <div className={style.editor_top_right}>
              <Button onClick={publishHandler} type="primary" htmlType="submit">
                发布
              </Button>

            <div className={style.right_p}>
              <Dropdown overlay={menu} placement="bottomRight">
                <span>...</span>
              </Dropdown>
            </div>
          </div>
        </Form>
        {/* 抽屉 */}
        <Drawer
          title={`页面属性`}
          placement="right"
          onClose={onClose}
          open={open}
          footer={
            <Space>
              <Button type="primary" onClick={onClose}>
                确认
              </Button>
            </Space>
          }
        >
          {/* 表单s */}
          <Form
            form={form}
          >
            <Form.Item label="封面">
              <Input placeholder="请输入页面封面" />
            </Form.Item>
            <Form.Item label="路径" name="path">
              <Input placeholder="请配置页面路径" value={notes.path} onChange={(e)=>{
                // console.log(e.target.value);
                // notes.path=e.target.value
                setNotes({
                  ...notes,
                  path:e.target.value
                })
              }} />
            </Form.Item>
            <Form.Item label="顺序" name="views" rules={[{ type: 'number', min: 0, max: 99 }]}>
              <InputNumber value={notes.views} onChange={(e)=>{
                console.log(e,'eeeee');
                setNotes({
                  ...notes,
                  views:e
                })
              }} />
            </Form.Item>
          </Form>


          {/* 表单e */}

        </Drawer>
      </div>

      {/* <div dangerouslySetInnerHTML={{ __html: pages.html }} /> */}
      <div>
        {/* 编辑 */}
        <MDEditor
          value={value}
        >
          <MDEditor.Markdown source={value} />
        </MDEditor>
      </div>
    </div>
  )
}

const mapStateToProps = ({ pages }: any) => {
  console.log(pages, '5555');


  return {
    pages,
  }
}
export default connect(mapStateToProps)(Edit)