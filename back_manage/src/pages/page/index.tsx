/* 页面管理 */

import React, { FC, useState } from 'react';
import { history } from 'umi';
import { Button, Pagination, Popconfirm, Breadcrumb } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { RedoOutlined } from '@ant-design/icons';
import { Space, Modal } from 'antd';
import style from './less/index.less';
import { connect } from 'dva';
import {
  ProFormSelect,
  ProFormText,
  QueryFilter,
} from '@ant-design/pro-components';
import moment from 'moment';
import AffixHead from '@/components/affixHead';
import Table_com from '@/components/table_com';
//引入组件
interface valuesType {
  name?: string;
  type?: string;
  status?: string;
}
const Page: FC = ({ pages, dispatch }: any) => {
  //访问量
  const [viewss, setViews] = useState();
  interface DataType {
    key: React.Key;
    name: string;
    age: number;
    address: string;
    id: string;
  }

  const columns: ColumnsType<DataType> = [
    {
      title: '名称',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: '路径',
      key: 'path',
      dataIndex: 'path',
    },
    {
      title: '顺序',
      key: 'order',
      dataIndex: 'order',
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'views',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      render: (_: any, record: any) => (
        <Space size="middle">
          <span>
            {record.status == 'publish' ? (
              <span>
                <span style={{ color: '#52c41a' }}>● </span>已发布
              </span>
            ) : (
              <span>
                <span style={{ color: '#faad14' }}>● </span>草稿
              </span>
            )}
          </span>
        </Space>
      ),
    },
    {
      title: '发布时间',
      dataIndex: 'createAt',
      key: 'createAt',
      render: (text) => {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '操作',
      key: 'action',
      render: (_, record: any) => (
        <Space size="middle">
          <a
            onClick={() => {
              editorHandler(record);
            }}
          >
            编辑
          </a>
          <a
            onClick={() =>
              changeDraft(
                record.id,
                record.status === 'publish' ? 'draft' : 'publish',
              )
            }
          >
            {record.status === 'publish' ? '下线' : '发布'}
          </a>
          <a onClick={() => showModal(record.views)}>查看访问</a>
          <Popconfirm
            title="确认要删除吗?"
            onConfirm={() => {
              deleteHandler(record.id);
            }}
            okText="Yes"
            cancelText="No"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
      width: 350,
      fixed: 'right',
    },
  ];
  // 搜索数据
  const [name, setName] = useState('');
  const [path, setPath] = useState('');
  const [status, setStatus] = useState('');
  const [loading, setLoading] = useState(false);
  const [disbutton, setDisbutton] = useState(false);
  //批量发布
  const [idd, setId] = useState([{}]);
  // 弹框
  const [isModalOpen, setIsModalOpen] = useState(false);
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: DataType[]) => {
      setId(selectedRowKeys);
      if (selectedRows.length) {
        setDisbutton(true);
      } else {
        setDisbutton(false);
      }
    },
  };
  // 搜索
  const searchHander = async (values: valuesType) => {
    if (values.name !== undefined) {
      setName(values.name);
    }
    if (values.type !== undefined) {
      setPath(values.type);
    }
    if (values.status !== undefined) {
      setStatus(values.status);
    }
    dispatch({
      type: 'pages/getRemote',
      payload: {
        page: 1,
        pageSize: 5,
        name: values.name ? values.name : '',
        path: values.type ? values.type : '',
        status: values.status ? values.status : '',
      },
    });
  };
  //分页
  //点击分页
  const paginationHanderle = (page: number, pageSize: number) => {
    dispatch({
      type: 'pages/getRemote',
      payload: {
        page,
        pageSize,
        name: name,
        path: path,
        status: status,
      },
    });
  };
  //改变每个条数
  const pageSizeHanderle = (current: number, size: number) => {
    console.log(current, size);
    dispatch({
      type: 'pages/getRemote',
      payload: {
        page: current,
        pageSize: size,
        name: name,
        path: path,
        status: status,
      },
    });
  };
  //删除
  const deleteHandler = (id: string) => {
    dispatch({
      type: 'pages/delete',
      payload: {
        id,
      },
    });
  };
  //点击编辑
  const editorHandler = (record: any) => {
    history.push(`/page/editor/${record.id}`, {
      state: record,
    });
  };
  // 点击发布或下线按钮
  const changeDraft = (id: string, drafts: string) => {
    dispatch({
      type: 'pages/draft',
      payload: {
        id: id,
        datas: { status: drafts },
      },
    });
  };
  //点击批量发布按钮
  const changeDrafts = () => {
    idd.forEach((item) => {
      setTimeout(() => {
        dispatch({
          type: 'pages/drafts',
          payload: {
            id: item,
            datas: { status: 'publish' },
          },
        });
      }, 500);
    });
  };
  //点击批量下线按钮
  const changePublish = () => {
    idd.forEach((item) => {
      setTimeout(() => {
        dispatch({
          type: 'pages/drafts',
          payload: {
            id: item,
            datas: { status: 'draft' },
          },
        });
      }, 500);
    });
  };
  //点击批量删除
  const changeDetele = () => {
    idd.forEach((item) => {
      setTimeout(() => {
        dispatch({
          type: 'pages/delete',
          payload: {
            id: item,
          },
        });
      }, 500);
    });
  };
  // 弹框
  const showModal = (views: any) => {
    setViews(views);
    setIsModalOpen(true);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className={style.page_wrap}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/page">页面管理</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <div>
          {/* from表单搜索s */}
          <div className={style.page_search}>
            <QueryFilter<{
              name: string;
              company: string;
            }>
              onFinish={searchHander}
            >
              <ProFormText name="name" label="名称" width={'md'} />
              <ProFormText name="type" label="路径" width={'md'} />
              <ProFormSelect
                width={'xs'}
                name="status"
                label="状态"
                showSearch
                valueEnum={{
                  publish: '已发布',
                  draft: '草稿',
                }}
              />
            </QueryFilter>
          </div>

          {/* from表单搜索e */}
        </div>

        <div className={style.page_jian}>
          <div className={style.page_button_dis}>
            {disbutton ? (
              <div>
                <Button onClick={changeDrafts} loading={loading} type="primary">
                  发布
                </Button>
                <Button onClick={changePublish} loading={loading}>
                  下线
                </Button>
                <Button
                  onClick={changeDetele}
                  loading={loading}
                  danger
                  type="primary"
                >
                  删除
                </Button>
              </div>
            ) : (
              <div></div>
            )}
          </div>
          <div>
            <Button
              className={style.button_width}
              type="primary"
              onClick={() => history.push('/page/editor')}
            >
              +新建
            </Button>
            <RedoOutlined
              onClick={() => {
                dispatch({
                  type: 'pages/getRemote',
                  payload: {
                    page: 1,
                    pageSize: 5,
                  },
                });
              }}
            />
          </div>
        </div>

        <Table_com
          columns={columns}
          dataSource={pages[0]}
          rowSelection={rowSelection}
        />
        {/* 分页 */}
        <div className={style.pagination}>
          <Pagination
            total={pages[1]}
            showTotal={() => `共 ${pages[1]} 条`}
            defaultPageSize={5}
            defaultCurrent={1}
            pageSizeOptions={[5, 10, 15]}
            showSizeChanger
            onChange={paginationHanderle}
            onShowSizeChange={pageSizeHanderle}
          />
        </div>
        {/* 弹框 */}
        <Modal
          footer={null}
          title="访问统计"
          open={isModalOpen}
          onCancel={handleCancel}
        >
          <p style={{ height: '200px' }}>{viewss}</p>
        </Modal>
      </main>
    </div>
  );
};
const mapStateToProps = ({ pages }: any) => {
  return {
    pages,
  };
};
export default connect(mapStateToProps)(Page);
