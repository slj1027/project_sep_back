/* 标签管理 */

import React, { FC, useState } from 'react';
import { Breadcrumb, Form, Button, Input, Popconfirm } from 'antd';
import { useRequest } from 'ahooks';
import { addTags, delTags, getTags, updateTags } from '@/api';
import style from './less/index.less';
import AffixHead from '@/components/affixHead';
interface Tags {
  articleCount: number;
  createAt: string;
  id: string;
  label: string;
  updateAt: string;
  value: string;
}
const Tags: FC = () => {
  const { data, run } = useRequest(getTags);
  const [form] = Form.useForm();
  const [action, setAction] = useState<Tags | undefined>();
  const actions = (item: any) => {
    setAction(item);
    form.setFieldsValue({ ...item });
  };
  const onFinish = (value: any) => {
    if (!action) {
      addTags(value).then((res) => {
        run();
      });
    } else {
      updateTags(action.id, { ...value }).then((res) => {
        run();
      });
    }
    form.resetFields();
  };
  const del = () => {
    delTags(action?.id as string).then((res) => {
      run();
      form.resetFields();
    });
  };
  return (
    <div className={style.tags}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/tags">标签管理</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <div className={style['ant-row']}>
          <div className={style['ant-col']}>
            <h3>添加标签</h3>
            <Form onFinish={onFinish} form={form}>
              <Form.Item name={'label'}>
                <Input placeholder="输入标签名称" />
              </Form.Item>
              <Form.Item name={'value'}>
                <Input placeholder="输入标签值（请输入英文，作为路由使用）" />
              </Form.Item>
              <Form.Item>
                {!action ? (
                  <Button type="primary" htmlType="submit">
                    保存
                  </Button>
                ) : (
                  <div>
                    <Button type="primary" htmlType="submit">
                      更新
                    </Button>
                    <Button
                      onClick={() => {
                        setAction(undefined);
                        form.setFieldsValue({ label: '', value: '' });
                      }}
                    >
                      返回添加
                    </Button>
                    <Popconfirm
                      title="确认要删除吗？"
                      onConfirm={()=>del}
                      okText="Yes"
                      cancelText="No"
                    >
                      <Button danger>
                        删除
                      </Button>
                    </Popconfirm>
                  </div>
                )}
              </Form.Item>
            </Form>
          </div>
          <div className={style['ant-col']}>
            <h3>所有标签</h3>
            <div className={style.allTags}>
              {data &&
                data.data &&
                data.data.length > 0 &&
                data.data.map((item: any) => {
                  return (
                    <span
                      key={item.id}
                      className={style.tags_item}
                      onClick={() => actions(item)}
                    >
                      {item.label}
                    </span>
                  );
                })}
            </div>
          </div>
        </div>
      </main>
    </div>
  );
};
export default Tags;
