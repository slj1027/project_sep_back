import React, { useState } from 'react';
import style from '../less/index.less';
import { Input, Select, Button } from 'antd';
const { Option } = Select;
interface PropType {
  page: number;
  pageSize: number;
  changPage: () => void;
  run: (params: any) => void;
}
function SearchBox({ page, pageSize, changPage, run }: PropType) {
  //*搜索
  const [from, setFrom] = useState('');
  const [to, setTo] = useState('');
  const [subject, setSubject] = useState('');
  const clean = () => {
    setFrom('');
    setTo('');
    setSubject('');
    run({ page, pageSize });
  };
  return (
    <div className={style.search}>
      <div className={style.searchInp}>
        <div>
          <span>发件人：</span>
          <Input
            type="text"
            placeholder="请输入称呼"
            onChange={(e) => {
              setFrom(e.target.value);
            }}
            value={from}
          />
        </div>
        <div>
          <span>收件人：</span>
          <Input
            type="text"
            placeholder="请输入联系方式"
            onChange={(e) => {
              setTo(e.target.value);
            }}
            value={to}
          />
        </div>
        <div>
          <span>主题：</span>
          <Input
            type="text"
            placeholder="请输入联系方式"
            onChange={(e) => {
              setSubject(e.target.value);
            }}
            value={subject}
          />
        </div>
      </div>
      <div className={style.searchBut}>
        <Button onClick={() => clean()}>重置</Button>
        <Button
          type="primary"
          onClick={() => {
            changPage();
            run({ page, pageSize, from, to, subject });
          }}
        >
          搜索
        </Button>
      </div>
    </div>
  );
}

export default SearchBox;
