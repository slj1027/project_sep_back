/* 邮件管理 */
import React, { FC, useEffect, useState } from 'react';
import { getEmail, delEmail } from '@/api';
import { useRequest } from 'ahooks';
import SearchBox from './components/searchBox';
import Table_com from '@/components/table_com';
import Pagination_com from '@/components/pagination_com';
import { Space, Spin, Button, Breadcrumb, Popconfirm } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import style from './less/index.less';
import AffixHead from '@/components/affixHead';
import moment from 'moment';
const Email: FC = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { loading, data, run } = useRequest((params) => getEmail(params), {
    manual: true,
  });

  useEffect(() => {
    run({ page, pageSize });
  }, []);
  const confirm = (id: string) => {
    delEmail(id).then(() => {
      run({ page, pageSize });
    });
  };
  const cancel = (e: React.MouseEvent<HTMLElement>) => {};
  //*渲染
  const columns = [
    {
      title: '发件人',
      dataIndex: 'from',
      key: 'from',
      width: 260,
    },
    {
      title: '收件人',
      dataIndex: 'to',
      key: 'to',
      width: 260,
    },
    {
      title: '主题',
      dataIndex: 'subject',
      key: 'subject',
      width: 260,
    },
    {
      title: '发送时间',
      dataIndex: 'createAt',
      key: 'createAt',
      width: 260,
      render(text: string) {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      width: 240,
      render: (_: any, record: any) => (
        <Space size="middle">
          <Popconfirm
            title="确认要删除吗？"
            onConfirm={() => confirm(record.id)}
            onCancel={() => cancel}
            okText="Yes"
            cancelText="No"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any) => {
      setCheck(selectedRowKeys);
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.name === 'Disabled User', //* Column configuration not to be checked
      name: record.name,
    }),
  };
  const [check, setCheck] = useState<any>([]);
  const onChange = (page: number, pageSize: number) => {
    setPage(page), setPageSize(pageSize);
  };
  const moreDel = () => {};
  return (
    <div className={style.poster}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/email">邮件管理</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        {/* <OssUpload></OssUpload> */}
        <SearchBox
          {...{ page, pageSize, run, changPage: () => setPage(1) }}
        ></SearchBox>
        <div>
          <div className={style.action}>
            <div>
              {check && check.length > 0 && (
                <div>
                  <Popconfirm
                    title="Are you sure to delete this task?"
                    onConfirm={() => {
                      check.forEach((item: string) => {
                        delEmail(item).then(() => {
                          run({ page, pageSize });
                        });
                      });
                      setCheck([])
                    }}
                    onCancel={() => cancel}
                    okText="Yes"
                    cancelText="No"
                  >
                    <Button type="primary" danger onClick={moreDel}>
                      删除
                    </Button>
                  </Popconfirm>
                </div>
              )}
            </div>
            <div className={style.reload}>
              <ReloadOutlined onClick={() => run({ page, pageSize })} />
            </div>
          </div>
          {loading ? (
            <Spin></Spin>
          ) : (
            <>
              <Table_com
                {...{ dataSource: data?.data[0], columns, rowSelection }}
              ></Table_com>
              <Pagination_com
                {...{ current: page, total: data?.data[1], onChange, pageSize }}
              ></Pagination_com>
            </>
          )}
        </div>
      </main>
    </div>
  );
};
export default Email;
