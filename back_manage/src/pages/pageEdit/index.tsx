/* 新建——新建文章 */
import { FC } from 'react';
import MarkdownEdit from '@/components/markdownEdit';
const PageEdit: FC = () => {
  return <MarkdownEdit url={'page'} />;
};

export default PageEdit;
