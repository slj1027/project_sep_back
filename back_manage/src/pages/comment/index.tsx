/* 评论管理 */

import { getComment, postComment, replyComment, delComment } from '@/api';
import { FC, useEffect, useState } from 'react';
import {
  Dropdown,
  Menu,
  Space,
  Popconfirm,
  message,
  Button,
  Spin,
  Badge,
  Breadcrumb,
} from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import moment from 'moment';
import { replyItemData } from './types';
import { responseType, userData } from '@/types';
import Table_com from '@/components/table_com';
import Pagination_com from '@/components/pagination_com';
import SearchBox from './components/SearchBox';
import ModalBox from '@/components/modal';
import AffixHead from '@/components/affixHead';
import style from './less/index.less';
import { useRequest } from 'ahooks';
const Comment: FC = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const [reply, setReply] = useState<replyItemData>();
  const [delId, setDelId] = useState('');
  const { loading, data, run } = useRequest((params) => getComment(params), {
    manual: true,
  });
  useEffect(() => {
    run({ page, pageSize });
  }, [page, pageSize]);
  //*删除
  const confirm = () => {
    delComment(delId).then((res: responseType) => {
      if (res.statusCode === 200) {
        run({ page, pageSize });
        message.success('删除成功');
      }
    });
  };
  //*渲染
  const columns = [
    {
      title: '状态',
      dataIndex: 'pass',
      key: 'pass',
      width: 100,
      render: (text: boolean) =>
        text ? (
          <Badge color={'green'} text={'通过'} />
        ) : (
          <Badge color={'yellow'} text={'未通过'} />
        ),
    },
    {
      title: '称呼',
      dataIndex: 'name',
      key: 'name',
      width: 100,
    },
    {
      title: '联系方式',
      dataIndex: 'email',
      key: 'email',
      width: 140,
    },
    {
      title: '原始内容',
      dataIndex: 'content',
      width: 120,
      key: 'content',
      render: (text: string) => (
        <Dropdown
          overlay={
            <Menu
              items={[
                {
                  key: '1',
                  label: <div>{text}</div>,
                },
              ]}
            />
          }
        >
          <a onClick={(e) => e.preventDefault()}>
            <Space>查看内容</Space>
          </a>
        </Dropdown>
      ),
    },
    {
      title: 'html内容',
      dataIndex: 'html',
      width: 120,
      key: 'html',
      render: (text: string) => (
        <Dropdown
          overlay={
            <Menu
              items={[
                {
                  key: '1',
                  label: <div>{text}</div>,
                },
              ]}
            />
          }
        >
          <a onClick={(e) => e.preventDefault()}>
            <Space>查看内容</Space>
          </a>
        </Dropdown>
      ),
    },
    {
      title: '管理文章',
      dataIndex: 'url',
      width: 120,
      key: 'url',
      render: (text: string) => (
        <Dropdown
          overlay={
            <Menu
              items={[
                {
                  key: '1',
                  label: <div>{text}</div>,
                },
              ]}
            />
          }
        >
          <a onClick={(e) => e.preventDefault()}>
            <Space>查看内容</Space>
          </a>
        </Dropdown>
      ),
    },
    {
      title: '创建时间',
      dataIndex: 'createAt',
      key: 'createAt',
      width: 260,
      render(text: string) {
        return moment(text).format('YYYY-MM-DD HH:mm:ss');
      },
    },
    {
      title: '父级评论',
      dataIndex: 'replyUserName',
      width: 120,
      key: 'replyUserName',
      render: (text: string) => (text ? text : '无'),
    },
    {
      title: '操作',
      key: 'action',
      fixed: 'right',
      width: 240,
      render: (_: any, record: any) => (
        <Space size="middle">
          <span
            onClick={() => {
              postComment(record.id, { pass: true }).then(
                (res: responseType) => {
                  if (res.statusCode === 200) {
                    run({ page, pageSize });
                  }
                },
              );
            }}
          >
            <a>通过</a>
          </span>
          <span
            onClick={() => {
              postComment(record.id, { pass: false }).then(
                (res: responseType) => {
                  if (res.statusCode === 200) {
                    run({ page, pageSize });
                  }
                },
              );
            }}
          >
            <a>拒绝</a>
          </span>
          <span
            onClick={() => {
              showModal();
              setReply(record);
            }}
          >
            <a>回复</a>
          </span>
          <Popconfirm
            title="确认要删除吗？"
            onConfirm={confirm}
            okText="Yes"
            cancelText="No"
          >
            <a href="#" onClick={() => setDelId(record.id)}>
              删除
            </a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  //*切换页码
  const change = (page: number, pageSize: number) => {
    setPage(page);
    setPageSize(pageSize);
  };

  //*回复
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = (value: string) => {
    setIsModalOpen(false);
    let user = JSON.parse(localStorage.getItem('user') as string) as userData;
    replyComment({
      content: value,
      createByAdmin: user.role === 'admin' ? true : false,
      email: user.email,
      hostId: reply?.hostId as string,
      name: user.name,
      parentCommentId: reply?.id as string,
      replyUserEmail: reply?.email as string,
      replyUserName: reply?.name as string,
      url: reply?.url as string,
    }).then((res: responseType) => {
      if (res.statusCode >= 200 && res.statusCode < 300) {
        run({ page, pageSize });
      }
    });
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  //*批量
  const [check, setCheck] = useState<React.Key[]>();
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[]) => {
      setCheck(selectedRowKeys);
    },
  };
  //*通过/拒绝
  const morePass = (flag: boolean) => {
    check?.forEach((item) => {
      console.log(item);

      postComment(item.toString(), { pass: flag }).then((res: responseType) => {
        if (res.statusCode === 200) {
          run({ page, pageSize });
        }
      });
    });
  };
  //*删除
  const moreDel = () => {
    check?.forEach((item) => {
      delComment(item.toString()).then((res: responseType) => {
        if (res.statusCode === 200) {
          run({ page, pageSize });
          message.success('删除成功');
        }
      });
    });
  };

  return (
    <div className={style.commentTable}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/comment">评论管理</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <SearchBox
          {...{
            page,
            pageSize,
            changPage: () => setPage(1),
            run,
          }}
        />
        <div>
          <div className={style.action}>
            <div>
              {check && check.length > 0 && (
                <div>
                  <Button type="primary" onClick={() => morePass(true)}>
                    通过
                  </Button>
                  <Button onClick={() => morePass(false)}>拒绝</Button>
                  <Button type="primary" danger onClick={moreDel}>
                    删除
                  </Button>
                </div>
              )}
            </div>
            <div className={style.reload}>
              <ReloadOutlined onClick={() => run({ page, pageSize })} />
            </div>
          </div>
          {!loading && data ? (
            <>
              <Table_com
                dataSource={data.data[0]}
                columns={columns}
                rowSelection={rowSelection}
              ></Table_com>
              <Pagination_com
                pageSize={pageSize}
                total={data.data[1] as number}
                current={page}
                onChange={change}
              ></Pagination_com>
            </>
          ) : (
            <div style={{ textAlign: 'center' }}>
              <Spin />
            </div>
          )}
        </div>
      </main>
      <ModalBox
        {...{ isModalOpen, showModal, handleOk, handleCancel }}
      ></ModalBox>
    </div>
  );
};
export default Comment;
