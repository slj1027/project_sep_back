//评论
export interface commentData {
  content: string;
  createAt: string;
  email: string| null;
  hostId: string;
  html: string;
  id: string;
  name: string;
  parentCommentId: string|null;
  pass: boolean;
  replyUserEmail: string|null;
  replyUserName: string|null;
  updateAt: string;
  url: string;
  userAgent: string;
  key?: string;
}
//回复
export interface replyItemData {
  content: string;
  createAt: string;
  email: string;
  hostId: string;
  html: string;
  id: string;
  name: string;
  parentCommentId: string | null;
  pass: boolean;
  replyUserEmail: string | null;
  replyUserName: string | null;
  updateAt: string;
  url: string;
  userAgent: string;
}
export interface replyData {
  content: string;
  createByAdmin: boolean;
  email: string;
  hostId: string;
  name: string;
  parentCommentId: string;
  replyUserEmail: string;
  replyUserName: string;
  url: string;
}
//搜索
export interface searchData {
  name?: string;
  email?: string;
  pass?: number;
  page: number;
  pageSize?: number;
}