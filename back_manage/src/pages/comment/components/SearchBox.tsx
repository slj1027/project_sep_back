import React, { useState } from 'react';
import style from '../less/index.less';
import { Input, Select, Button } from 'antd';
const { Option } = Select;
interface PropType {
  page: number;
  pageSize: number;
  changPage: () => void;
  run: (params: any) => void;
}
function SearchBox({ page, pageSize, changPage, run }: PropType) {
  //*搜索
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [pass, setPass] = useState('');
  const handleChange = (value: string) => {
    setPass(value);
  };
  const clean = () => {
    setName('');
    setEmail('');
    setPass('');
    run({ page, pageSize });
  };
  return (
    <div className={style.search}>
      <div className={style.searchInp}>
        <div>
          <span>称呼：</span>
          <Input
            type="text"
            placeholder="请输入称呼"
            onChange={(e) => {
              setName(e.target.value);
            }}
            value={name}
          />
        </div>
        <div>
          <span>Email：</span>
          <Input
            type="text"
            placeholder="请输入联系方式"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            value={email}
          />
        </div>
        <div>
          <span>状态：</span>
          <Select value={pass} style={{ width: 120 }} onChange={handleChange}>
            <Option value="通过">通过</Option>
            <Option value="未通过">未通过</Option>
          </Select>
        </div>
      </div>
      <div className={style.searchBut}>
        <Button onClick={() => clean()}>重置</Button>
        <Button
          type="primary"
          onClick={() => {
            changPage();
            run({ page, pageSize, name, email, pass: pass === '通过' ? 1 : 0 });
          }}
        >
          搜索
        </Button>
      </div>
    </div>
  );
}

export default SearchBox;
