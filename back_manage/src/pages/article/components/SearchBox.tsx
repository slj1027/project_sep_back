import React, { useState } from 'react';
import { Input, Select, Button } from 'antd';
import { useRequest } from 'ahooks';
import { getCategory } from '@/api';
import style from '../less/index.less'
const { Option } = Select;
interface PropType {
  page: number;
  pageSize: number;
  changPage: () => void;
  run: (params: any) => void;
}
function SearchBox({ page, pageSize, changPage, run }: PropType) {
  //*搜索
  const [title, setTitle] = useState('');
  const [email, setEmail] = useState('');
  const [pass, setPass] = useState('');
  const handleChange = (value: string) => {
    setPass(value);
  };
  const clean = () => {
    setTitle('');
    setEmail('');
    setPass('');
    run({ page, pageSize });
  };
  const { data } = useRequest(getCategory);

  return (
    <div className={style.search}>
      <div className={style.searchInp}>
        <div>
          <span>标题：</span>
          <Input
            type="text"
            placeholder="请输入标题"
            onChange={(e) => {
              setTitle(e.target.value);
            }}
            value={title}
          />
        </div>
        <div>
          <span>状态：</span>
          <Select value={pass} style={{ width: 120 }} onChange={handleChange}>
            <Option value="草稿">草稿</Option>
            <Option value="已发布">已发布</Option>
          </Select>
        </div>
        <div>
          <span>状态：</span>
          <Select value={pass} style={{ width: 120 }} onChange={handleChange}>
            {data &&
              data.data.length > 0 &&
              data.data.map((item: any) => {
                return <Option value={item.label}>{item.label}</Option>;
              })}
          </Select>
        </div>
      </div>
      <div className={style.searchBut}>
        <Button onClick={() => clean()}>重置</Button>
        <Button
          type="primary"
          onClick={() => {
            changPage();
            run({ page, pageSize, name, email, pass: pass === '通过' ? 1 : 0 });
          }}
        >
          搜索
        </Button>
      </div>
    </div>
  );
}

export default SearchBox;
