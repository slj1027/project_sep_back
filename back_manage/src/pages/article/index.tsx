/* 文章管理 —— 所有文章 */
import React, { FC, Key, useEffect, useRef, useState } from 'react';
import { ProTable } from '@ant-design/pro-components';
import { ReloadOutlined } from '@ant-design/icons';
import { Badge, Tag, Space, Image, Button, Modal, Breadcrumb } from 'antd';
import moment from 'moment';
import { history } from 'umi';
import { useRequest } from 'ahooks';
import { get_article } from '@/api';
import BaseSelect from '@/components/baseSelects';
import { edit_article, del_article } from '../../api/index';
import style from './less/allarticle.less';
import AffixHead from '@/components/affixHead';
interface ActionType {
  reload: (resetPageIndex?: boolean) => void;
  reloadAndRest: () => void;
  reset: () => void;
  clearSelected?: () => void;
  startEditable: (rowKey: Key) => boolean;
  cancelEditable: (rowKey: Key) => boolean;
}
const Article: FC = () => {
  const { runAsync } = useRequest(get_article, {
    manual: true,
  });
  const [labelList, setlabelList]: any = useState([]);
  const [modal2Open, setModal2Open] = useState(false);
  const getlabelList = async () => {
    const res = await get_article();
    const list: any = [];
    res.data[0].forEach((item: any) => {
      if (item.category) {
        list.push({
          title: item.category.label,
          key: item.category.id,
        });
      }
    });
    setlabelList(list);
  };
  useEffect(() => {
    getlabelList();
  }, []);

  const ref = useRef<ActionType>();
  const columns = [
    {
      title: '标题',
      dataIndex: 'title',
      key: 'title',
      render: (text: any) => (
        <a
          style={{
            color: '#000',
          }}
        >
          {text}
        </a>
      ),
      fixed: 'left',
      width: '10%',
    },
    {
      title: '状态',
      dataIndex: 'status',
      key: 'status',
      width: '10%',
      valueType: 'select',
      valueEnum: {
        publish: { text: '已发布' },
        nopublish: { text: '草稿' },
      },
      render: (text: any, record: any) => (
        <span>
          {record.status === 'publish' ? (
            <Badge status="success"></Badge>
          ) : (
            <Badge status="error"></Badge>
          )}
          {record.status === 'publish' ? '已发布' : '草稿'}
        </span>
      ),
    },
    {
      title: '分类',
      dataIndex: 'category',
      key: 'category',
      width: '10%',
      valueType: 'select',
      renderFormItem: (item: any, { type, defaultRender, ...rest }: any) => {
        return (
          <BaseSelect title="分类" {...rest} options={labelList}></BaseSelect>
        );
      },
      render: (
        text: any,
        record: {
          category: {
            label:
              | boolean
              | React.ReactChild
              | React.ReactFragment
              | React.ReactPortal
              | null
              | undefined;
          };
        },
        _: any,
        action: any,
      ) => (
        <span>
          {record.category ? (
            <Tag color="blue">{record.category.label}</Tag>
          ) : (
            ''
          )}
        </span>
      ),
    },
    {
      title: '标签',
      key: 'tags',
      dataIndex: 'tags',
      width: '10%',
      hideInSearch: true,
      render: (_: any, { tags }: any) => (
        <>
          {tags.map((tag: any, index: number) => {
            return (
              <Tag key={index} color="pink">
                {tag.label}
              </Tag>
            );
          })}
        </>
      ),
    },
    {
      title: '阅读量',
      dataIndex: 'views',
      key: 'views',
      width: '10%',
      hideInSearch: true,
      render: (views: any) => (
        <Tag
          style={{
            borderRadius: '50%',
          }}
          color="green"
        >
          {views}
        </Tag>
      ),
    },
    {
      title: '喜欢数',
      dataIndex: 'likes',
      key: 'likes',
      width: '10%',
      hideInSearch: true,
      render: (likes: any) => (
        <Tag
          style={{
            borderRadius: '50%',
          }}
          color="red"
        >
          {likes}
        </Tag>
      ),
    },
    {
      title: '发布时间',
      dataIndex: 'publishAt',
      key: 'publishAt',
      width: '15%',
      hideInSearch: true,
      render: (time: any) => (
        <span style={{ display: 'inline-block', width: '200px' }}>
          {moment(time).format('YYYY-MM-DD hh:mm:ss')}
        </span>
      ),
    },
    {
      title: '操作',
      dataIndex: 'isRecommended',
      key: 'isRecommended',
      fixed: 'right',
      width: '16%',
      hideInSearch: true,
      render: (text: any, record: any) => (
        <Space size="middle" style={{ width: '250px' }}>
          <a
            onClick={() =>
              history.push(`/article/editor/${record.id}`, {
                ...record,
              })
            }
          >
            编辑
          </a>
          <a
            onClick={() => {
              edit_article({
                id: record.id,
                isRecommended: !record.isRecommended,
              });
              ref?.current?.reload();
            }}
          >
            {text ? '撤销首焦' : '首焦推荐'}
          </a>
          {/* 查看 */}
          <a
            onClick={() => {
              setModal2Open(true);
            }}
          >
            查看访问
          </a>
          <a
            onClick={() => {
              del_article(record.id);
              ref?.current?.reload();
            }}
          >
            删除
          </a>
        </Space>
      ),
    },
  ];
  return (
    <div className={style.allarticle}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/article">所有文章</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <div className={style.allarticle_main}>
          <ProTable
            actionRef={ref}
            columns={columns}
            sticky
            scroll={{ x: 1500 }}
            rowSelection={{
              type: 'checkbox',
            }}
            rowKey={'id'}
            search={{
              searchText: '搜索',
              span: 5,
              optionRender: (searchConfig, formProps, dom) => {
                return dom;
              },
            }}
            toolbar={{
              settings: [
                <Button
                  type="primary"
                  onClick={() => {
                    history.push('/article/editor');
                  }}
                >
                  +新增
                </Button>,
                <ReloadOutlined
                  onClick={() => ref.current?.reload()}
                ></ReloadOutlined>,
              ],
            }}
            request={async (options: any) => {
              //初始化的创建表格执行，分页改变执行，点击查询的时候也会执行
              options = {
                ...options,
                page: options.current,
              };
              delete options.current;
              const { data } = await runAsync(options);
              console.log(data);

              return {
                data: data[0],
                success: true,
                total: data[1],
              };
            }}
            tableAlertRender={({ selectedRowKeys }) => {
              return (
                <Space size={16}>
                  <Button
                    onClick={() => {
                      selectedRowKeys.forEach((item) => {
                        edit_article({
                          id: item,
                          status: 'publish',
                        });
                      });
                      ref?.current?.reload();
                    }}
                  >
                    发布
                  </Button>
                  <Button
                    onClick={() => {
                      selectedRowKeys.forEach((item) => {
                        edit_article({
                          id: item,
                          status: 'draft',
                        });
                      });
                      ref?.current?.reload();
                    }}
                  >
                    草稿
                  </Button>
                  <Button
                    onClick={() => {
                      selectedRowKeys.forEach((item) => {
                        edit_article({
                          id: item,
                          isRecommended: true,
                        });
                      });
                      ref?.current?.reload();
                    }}
                  >
                    首焦推荐
                  </Button>
                  <Button
                    onClick={() => {
                      selectedRowKeys.forEach((item) => {
                        edit_article({
                          id: item,
                          isRecommended: false,
                        });
                      });
                      ref?.current?.reload();
                    }}
                  >
                    撤销首焦
                  </Button>
                  <Button
                    onClick={() => {
                      selectedRowKeys.forEach((item) => {
                        del_article(item as string);
                      });
                      ref?.current?.reload();
                    }}
                  >
                    删除
                  </Button>
                </Space>
              );
            }}
            pagination={{
              pageSize: 12,
              onChange: (page) => console.log(page),
            }}
          ></ProTable>

          {/* 查看访问 */}
          <Modal
            title="访问统计"
            centered
            visible={modal2Open}
            onOk={() => setModal2Open(false)}
            onCancel={() => setModal2Open(false)}
          >
            <Image
              className={style.img}
              src="https://img2.baidu.com/it/u=3465057997,3670928934&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"
            />
          </Modal>
        </div>
      </main>
    </div>
  );
};

export default Article;
