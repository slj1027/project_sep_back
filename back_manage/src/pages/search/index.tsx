/* 搜索记录 */
import { ReactChild, ReactFragment, ReactPortal, useState } from 'react';
import { Badge, Space, Popconfirm, message, Button, Breadcrumb } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import { useRequest } from 'ahooks';
import { MomentInput } from 'moment';
import moment from 'moment';
import AffixHead from '@/components/affixHead';
import Form from './components/Searchfrom';
import Table_com from '@/components/table_com';
import Pagination_com from '@/components/pagination_com';
import { delserch, getSearch } from '@/api';
import style from './less/index.less';
export default function search() {
  const { data, run } = useRequest(getSearch);

  let [page, setPage] = useState(1);
  let [pageSize, setPagesize] = useState(12);
  const columns = [
    {
      title: '搜索词',
      width: 100,
      dataIndex: 'keyword',
    },
    {
      title: '搜索量',
      width: 60,
      render: (
        text: any,
        record: {
          count:
            | boolean
            | ReactChild
            | ReactFragment
            | ReactPortal
            | null
            | undefined;
        },
      ) => (
        <Badge
          className="site-badge-count-109"
          count={record.count}
          style={{ backgroundColor: '#52c41a' }}
        />
      ),
    },
    {
      title: '搜索时间',
      dataIndex: 'createAt',
      width: 140,
      render: (text: MomentInput, record: any) =>
        moment(text).format('YYYY-MM-DD HH:mm:ss'),
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: 60,
      render: (text: any, record: { id: string }) => (
        <Space size="middle">
          <Popconfirm
            title="你确定要删除吗"
            onCancel={() => confirm()}
            onConfirm={() => {
              new Promise<void>((resolve, reject) => {
                delserch(record.id);
                resolve();
              }).then(() => {
                setTimeout(() => {
                  run({
                    page: page,
                    pageSize: pageSize,
                  });
                });
              });
              message.success('删除成功');
            }}
            okText="确定"
            cancelText="不确定"
          >
            <a>删除</a>
          </Popconfirm>
        </Space>
      ),
    },
  ];
  const [check, setSelectedRowKeys] = useState<string[]>([]);
  const onSelectChange = (newSelectedRowKeys: any[]) => {
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    check,
    onChange: onSelectChange,
  };
  const onChange = (page: number, pageSize: number) => {
    setPage(page);
    setPagesize(pageSize);
    run({
      page,
      pageSize,
    });
  };
  const onFinish = (values: any) => {
    setPage(1);
    run({
      ...values,
      page: 1,
      pageSize,
    });
  };
  return (
    <div className={style.search}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/search">搜索记录</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <Form onFinish={onFinish} />
        <div className={style.search_body}>
          <div className={style.action}>
            <div>
              {check && check.length > 0 && (
                <div>
                  <Button
                    type="primary"
                    danger
                    onClick={() => {
                      check.forEach((item) => {
                        delserch(item);
                      });
                      run({ page, pageSize });
                    }}
                  >
                    删除
                  </Button>
                </div>
              )}
            </div>
            <div className={style.reload}>
              <ReloadOutlined onClick={() => run({ page, pageSize })} />
            </div>
          </div>
          <Table_com
            dataSource={data?.data[0]}
            columns={columns}
            rowSelection={rowSelection}
          />
          <Pagination_com
            current={page}
            total={data?.data[1]}
            pageSize={pageSize}
            onChange={onChange}
          />
        </div>
      </main>
    </div>
  );
}
