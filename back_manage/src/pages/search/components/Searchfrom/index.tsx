import React from 'react';
import style from './index.less';
import { Form, Input, Button, Select } from 'antd';
interface PropType {
  onFinish: (params: any) => void;
}
const App = ({ onFinish }: PropType) => {
  const [form] = Form.useForm();
  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className={style.bgfrom}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="inline"
        form={form}
      >
        <Form.Item label="类型" name="type">
          <Input placeholder="请输入类型" value="" />
        </Form.Item>
        <Form.Item label="搜索词" name="keyword">
          <Input placeholder="请输入搜索词" value="" />
        </Form.Item>
        <Form.Item label="搜索量" name="count">
          <Input placeholder="请输入搜索量" value="" />
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button type="primary" htmlType="submit">
            搜索
          </Button>
        </Form.Item>
        <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
          <Button
            onClick={() => {
              form.resetFields();
            }}
          >
            重置
          </Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default App;
