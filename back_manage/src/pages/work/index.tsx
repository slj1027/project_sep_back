/* 工作台 */
import { FC } from 'react';
import { useRequest } from 'ahooks';
import { getChartData } from '@/api/exs';
import { Card, List } from 'antd';
import { Link } from 'umi';
import Comment from './comment/comment';
import Article from './Article/article';
import BaseChart from '@/components/baseChart/baseChart';
import style from './less/index.less';
import AffixHead from '@/components/affixHead';
const Work: FC = () => {
  const { loading, data } = useRequest(getChartData, {
    pollingInterval: 10000,
  });
  const user = JSON.parse(localStorage.getItem('user') as string);
  return (
    <div className={style.work}>
      <AffixHead offsetTop={48}>
        <div>
          <p>/工作台</p>
          <h1 style={{fontSize:'40px',fontWeight:"bolder"}}>您好：{user.name}</h1>
          <div>你的角色是:{user.role === 'admin' ? '管理员' : '访客'}</div>
        </div>
      </AffixHead>
      <div className={style['site-card-border-less-wrapper']}>
        {/* 图表 */}
        <Card title="面板导航" bordered={false}>
          {/* 
      type 用来指定echarts图表类型
       */}
          <BaseChart
            //图表类型
            type="line"
            charData={{
              title: {
                text: '每周用户访问指标',
              },
              legend: 'plain',
            }}
            series={[
              {
                type: 'line',
                name: '访问量',
                data: data?.data[0],
              },
              {
                type: 'bar',
                name: '成交量',
                data: data?.data[0],
              },
            ]}
            color={['#c23531', '#2f4554', '#61a0a8']}
            xData={['1', '2', '3', '4', '5', '6', '7', '8']}
            loading={loading}
            width={1200}
            height={300}
          />
        </Card>
        {/* 快速导航 */}
        <Card className={style.nav} title="快速导航" bordered={false}>
          <div className={style.navItem}>
            <Link to={'/article'}>文章管理</Link>
            <Link to={'/comment'}>评论管理</Link>
            <Link to={'/file'}>文件管理</Link>
            <Link to={'/user'}>用户管理</Link>
            <Link to={'/view'}>访问管理</Link>
            <Link to={'/setting'}>系统设置</Link>
          </div>
        </Card>
      </div>
      {/* 文章模块 */}
      <div className={style.article}>
        <Card
          className={style.art}
          title={
            <h4>
              <span>最新文章</span>
              <span>
                <Link to={'/article'}>所有文章</Link>
              </span>
            </h4>
          }
          bordered={false}
        >
          <Article />
        </Card>
      </div>
      {/* 最新评论 */}
      <div className={style.comment}>
        <Comment />
      </div>
    </div>
  );
};

export default Work;
