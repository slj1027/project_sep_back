import React, { FC } from 'react';
import { useRequest } from 'ahooks';
import { getArticle } from '@/api';
import { Card } from 'antd';
import style from './less/index.less';
const { Meta } = Card;
const Article: FC = () => {
  const { data } = useRequest(() =>
    getArticle({
      page: 1,
      pageSize: 6,
    }),
  );

  return (
    <div className={style.work_article}>
      {data &&
        data.data &&
        data.data[0].length > 0 &&
        data.data[0].map((item: any, index: any) => {
          return (
            <Card
              hoverable
              className={style.work_card_article}
              cover={
                item.cover ? (
                  <div
                    style={{
                      backgroundImage: 'url(' + item.cover + ')',
                      backgroundSize: 'cover',
                      backgroundPosition: '50%',
                      color: '#999',
                      fontSize: '30px',
                      width: '100%',
                      height: '160px',
                    }}
                  ></div>
                ) : (
                  <div
                    style={{
                      width: '100%',
                      height: '160px',
                    }}
                  >最新文章</div>
                )
              }
            >
              <Meta title={item.title} />
            </Card>
          );
        })}
    </div>
  );
};

export default Article;
