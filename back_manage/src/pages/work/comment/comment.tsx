import { FC, useState } from 'react';
import { useRequest } from 'ahooks';
import { Dropdown, Menu, List, message, Popconfirm, Badge } from 'antd';
import { _getUserLust } from '@/api/work';
import ModalBox from '@/components/modal';
import style from './comment.less';
import { responseType, userData } from '@/types';
import { delComment, postComment, replyComment } from '@/api';
interface replyItemData {
  content: string;
  createAt: string;
  email: string;
  hostId: string;
  html: string;
  id: string;
  name: string;
  parentCommentId: string | null;
  pass: boolean;
  replyUserEmail: string | null;
  replyUserName: string | null;
  updateAt: string;
  url: string;
  userAgent: string;
}
const comment: FC = () => {
  const { data, run } = useRequest(_getUserLust);
  const [delId, setDelId] = useState('');
  const confirm = () => {
    delComment(delId).then((res: responseType) => {
      if (res.statusCode === 200) {
        run();
        message.success('删除成功');
      }
    });
  };
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const [reply, setReply] = useState<replyItemData>();

  const handleOk = (value: string) => {
    setIsModalOpen(false);
    let user = JSON.parse(localStorage.getItem('user') as string) as userData;
    replyComment({
      content: value,
      createByAdmin: user.role === 'admin' ? true : false,
      email: user.email,
      hostId: reply?.hostId as string,
      name: user.name,
      parentCommentId: reply?.id as string,
      replyUserEmail: reply?.email as string,
      replyUserName: reply?.name as string,
      url: reply?.url as string,
    }).then((res: responseType) => {
      if (res.statusCode >= 200 && res.statusCode < 300) {
        run();
      }
    });
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className={style.comment}>
      {data && data.data && data.data[0].length > 0 && (
        <List
          header={
            <h4>
              <span>最新评论</span>
              <span>
                <a href={'/comment'}>全部评论</a>
              </span>
            </h4>
          }
          bordered
          dataSource={data.data[0]}
          renderItem={(item: any) => (
            <List.Item>
              <div>
                <span>{item.name}</span>
                <span>在</span>
                <span>
                  <Dropdown
                    overlay={
                      <Menu
                        items={[
                          {
                            key: '1',
                            label: <div>{item.url}</div>,
                          },
                        ]}
                      />
                    }
                  >
                    <a onClick={(e) => e.preventDefault()}>文章</a>
                  </Dropdown>
                </span>
                <span>评论</span>
                <span>
                  <Dropdown
                    overlay={
                      <Menu
                        items={[
                          {
                            key: '1',
                            label: (
                              <div
                                dangerouslySetInnerHTML={{ __html: item.html }}
                              ></div>
                            ),
                          },
                        ]}
                      />
                    }
                  >
                    <a onClick={(e) => e.preventDefault()}>查看内容</a>
                  </Dropdown>
                </span>
                <span>
                  {item.pass ? (
                    <Badge color={'green'} text={'通过'} />
                  ) : (
                    <Badge color={'yellow'} text={'未通过'} />
                  )}
                </span>
              </div>
              <div>
                <span
                  onClick={() => {
                    postComment(item.id, { pass: true }).then(
                      (res: responseType) => {
                        if (res.statusCode === 200) {
                          run();
                        }
                      },
                    );
                  }}
                >
                  <a>通过</a>
                </span>
                <span
                  onClick={() => {
                    postComment(item.id, { pass: false }).then(
                      (res: responseType) => {
                        if (res.statusCode === 200) {
                          run();
                        }
                      },
                    );
                  }}
                >
                  <a>拒绝</a>
                </span>
                <span
                  onClick={() => {
                    showModal();
                    setReply(item);
                  }}
                >
                  <a>回复</a>
                </span>
                <Popconfirm
                  title="确认要删除吗？"
                  onConfirm={confirm}
                  okText="Yes"
                  cancelText="No"
                >
                  <a href="#" onClick={() => setDelId(item.id)}>
                    删除
                  </a>
                </Popconfirm>
              </div>
            </List.Item>
          )}
        />
      )}
      <ModalBox
        {...{ isModalOpen, showModal, handleOk, handleCancel }}
      ></ModalBox>
    </div>
  );
};

export default comment;
