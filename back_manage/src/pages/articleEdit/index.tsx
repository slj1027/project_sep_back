/* 新建——新建文章 */
import { FC } from 'react';
import MarkdownEdit from '@/components/markdownEdit';

const ArticleEdit: FC = () => {
  return <MarkdownEdit url={'article'} />;
};

export default ArticleEdit;
