/* 访问记录 */
import React, { FC, useState } from 'react';
import type { ColumnsType } from 'antd/es/table';
import Table_com from '@/components/table_com';
import Pagination_com from '@/components/pagination_com';
import Form from './components/Searchfrom';
import AffixHead from '@/components/affixHead';
import { useRequest } from 'ahooks';
import { _delVisit, _getVisit } from '@/api/visit';
import { Col, Row, Spin, message, Popconfirm, Button, Breadcrumb } from 'antd';
import { ReloadOutlined } from '@ant-design/icons';
import { responseType } from '@/types';
import moment from 'moment';
import style from './less/index.less';
interface DataType {
  id: string;
  key: React.Key;
  name: string;
  age: number;
  address: string;
}
interface Data {
  data: any;
}
interface reqDataType {
  data: Data;
  run: (params: any) => void;
  loading: boolean;
}
const Visit: FC = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(10);
  const { data, run, loading } = useRequest(_getVisit) as reqDataType;
  const change = (page: number, pageSize: number) => {
    setPage(page);
    setPageSize(pageSize);
    run({ page, pageSize });
  };
  const serchfrom = (from: any) => {
    run({
      ...from,
      page,
      pageSize,
    });
  };
  const [selectedRowKeys, setSelectedRowKeys] = useState<React.Key[]>([]);
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[]) => {
      setSelectedRowKeys(selectedRowKeys);
    },
  };
  const moreDel = () => {
    selectedRowKeys?.forEach((item) => {
      _delVisit(item.toString()).then((res: responseType) => {
        if (res.statusCode === 200) {
          run({ page, pageSize });
          message.success('删除成功');
        }
      });
    });
  };
  const [delId, setDelId] = useState('');

  const confirm = () => {
    _delVisit(delId).then((res: responseType) => {
      if (res.statusCode === 200) {
        run({ page, pageSize });
        message.success('删除成功');
      }
    });
  };

  const columns: ColumnsType<DataType> = [
    {
      title: 'URL',
      dataIndex: 'url',
      render: (text) => <a href={text}>{text}</a>,
      width: 200,
    },
    {
      title: 'IP',
      dataIndex: 'ip',
      width: 140,
    },
    {
      title: '浏览器',
      dataIndex: 'browser',
      width: 140,
    },
    {
      title: '内核',
      dataIndex: 'engine',
      width: 140,
    },
    {
      title: '操作系统',
      dataIndex: 'os',
      width: 120,
    },
    {
      title: '设备',
      dataIndex: 'device',
      width: 100,
    },
    {
      title: '地址',
      dataIndex: 'address',
      width: 120,
    },
    {
      title: '访问量',
      dataIndex: 'count',
      width: 100,
      render: (text) => <div className={style.num}>{text}</div>,
    },
    {
      title: '访问时间',
      dataIndex: 'updateAt',
      width: 180,
      render: (text) => moment(text).format('YYYY-MM-DD HH:mm:ss'),
    },
    {
      title: '操作',
      dataIndex: 'action',
      fixed: 'right',
      width: 140,
      render: (_: any, record: any) => (
        <Popconfirm
          title="确认要删除吗？"
          onConfirm={confirm}
          okText="Yes"
          cancelText="No"
        >
          <a href="#" onClick={() => setDelId(record.id)}>
            删除
          </a>
        </Popconfirm>
      ),
    },
  ];
  return (
    <Row>
      <Col>
        <div className={style.commentTable}>
          <AffixHead offsetTop={48}>
            <Breadcrumb>
              <Breadcrumb.Item>
                <a href="/work">工作台</a>
              </Breadcrumb.Item>
              <Breadcrumb.Item>
                <a href="/visit">访问统计</a>
              </Breadcrumb.Item>
            </Breadcrumb>
          </AffixHead>
          <main>
            <div>
              <Form
                serchfrom={serchfrom}
                run={run}
                page={page}
                pageSize={pageSize}
              />
            </div>
            <div>
              <div style={{ marginBottom: 16 }}>
                <div className={style.action}>
                  <div>
                    {selectedRowKeys && selectedRowKeys.length > 0 && (
                      <div>
                        <Popconfirm
                          title="确认要删除吗？"
                          onConfirm={confirm}
                          okText="Yes"
                          cancelText="No"
                        >
                          <Button type="primary" danger onClick={moreDel}>
                            删除
                          </Button>
                        </Popconfirm>
                      </div>
                    )}
                  </div>
                  <div className={style.reload}>
                    <ReloadOutlined onClick={() => run({ page, pageSize })} />
                  </div>
                </div>
              </div>
              {!loading && data ? (
                <>
                  <Table_com
                    dataSource={data.data[0]}
                    columns={columns}
                    rowSelection={rowSelection}
                  ></Table_com>
                  <Pagination_com
                    pageSize={pageSize}
                    total={data.data[1] as number}
                    current={page}
                    onChange={change}
                  ></Pagination_com>
                </>
              ) : (
                <div style={{ textAlign: 'center' }}>
                  <Spin />
                </div>
              )}
            </div>
          </main>
        </div>
      </Col>
    </Row>
  );
};
export default Visit;
