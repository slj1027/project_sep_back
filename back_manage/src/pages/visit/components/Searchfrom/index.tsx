import React from 'react';
import style from './index.less';
import { Form, Input, Button, Select } from 'antd';
interface obj {
  [name: string]: string;
}
interface props {
  serchfrom(from: obj): void;
  page: number;
  pageSize: number;
  run: (params:any) => void;
}
const App = ({ serchfrom, page, pageSize, run }: props) => {
  const [formnf] = Form.useForm();
  const onFinish = (values: any) => {
    serchfrom(values);
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className={style.bgfrom}>
      <Form
        name="basic"
        labelCol={{ span: 8 }}
        wrapperCol={{ span: 16 }}
        initialValues={{ remember: true }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete="off"
        layout="inline"
        form={formnf}
      >
        <div className={style.search}>
          <Form.Item label="IP" name="ip">
            <Input placeholder="请输入ip" value="" />
          </Form.Item>
          <Form.Item label="UA" name="userAgent">
            <Input placeholder="请输入搜索词" value="" />
          </Form.Item>
          <Form.Item label="URL" name="url">
            <Input placeholder="请输入搜索量" value="" />
          </Form.Item>
          <Form.Item label="地址" name="address">
            <Input placeholder="请输入类型" value="" />
          </Form.Item>
          <Form.Item label="浏览器" name="browser">
            <Input placeholder="请输入搜索词" value="" />
          </Form.Item>
          <Form.Item label="内核" name="engine">
            <Input placeholder="请输入搜索量" value="" />
          </Form.Item>
          <Form.Item label="OS" name="os">
            <Input placeholder="请输入类型" value="" />
          </Form.Item>
          <Form.Item label="设备" name="device">
            <Input placeholder="请输入搜索词" value="" />
          </Form.Item>
        </div>

        <div className={style.butom}>
          <div></div>
          <div className={style.butombt}>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
            </Form.Item>
            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button
                onClick={() => {
                  formnf.resetFields();
                  run({ page, pageSize });
                }}
              >
                重置
              </Button>
            </Form.Item>
          </div>
        </div>
      </Form>
    </div>
  );
};
export default App;
