import React, { useEffect, useState } from 'react';
import { Avatar, Input, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { updateUser } from '@/api';
import Aside from '@/components/aside';
import style from './less/index.less'
function Setting() {
  const user = JSON.parse(localStorage.getItem('user') as string);
  const [name, setName] = useState(user.name);
  const [email, setEmail] = useState(user.email);
  const [open, setOpen] = useState(false);
  const [img, setImg] = useState(() => '');
  useEffect(() => {
    if (user && user.avatar) {
      setImg(user.avatar);
    }
  }, []);
  const onCheck = (img: string) => {
    setImg(img);
  };
  const showDrawer = () => {
    setOpen(true);
  };
  const onClose = () => {
    setOpen(false);
  };
  return (
    <div className={style.myInfo}>
      <div onClick={showDrawer} className={style.avatar}>
        {img ? (
          <img src={img} alt="用户头像" />
        ) : (
          <Avatar size="large" icon={<UserOutlined />} />
        )}
      </div>
      <div>
        <p>
          <span>{'用户名'}</span>
          <Input value={name} onChange={(e) => setName(e.target.value)} />
        </p>
        <p>
          <span>{'邮箱'}</span>
          <Input value={email} onChange={(e) => setEmail(e.target.value)} />
        </p>
      </div>
      <Button
        onClick={() => {
          user.email = email;
          user.name = name;
          user.avatar = img;
          updateUser({ ...user }).then((res) => {
            localStorage.setItem('user', JSON.stringify(res.data));
          });
        }}
      >
        保存
      </Button>
      <Aside {...{ open, onClose, onCheck }}></Aside>
    </div>
  );
}

export default Setting;
