import React, { useState } from 'react';
import { Card } from 'antd';
import Setting from './setting';
import UpdatePsw from './updatePsw';
function MyInfo() {
  const tabList = [
    {
      key: 'tab1',
      tab: '基本设置',
    },
    {
      key: 'tab2',
      tab: '更新密码',
    },
  ];

  const contentList: Record<string, React.ReactNode> = {
    tab1: <Setting />,
    tab2: <UpdatePsw />,
  };
  const [activeTabKey1, setActiveTabKey1] = useState<string>('tab1');
  const onTab1Change = (key: string) => {
    setActiveTabKey1(key);
  };
  return (
    <Card
      style={{ width: '100%',height:'auto' }}
      title="个人资料"
      tabList={tabList}
      activeTabKey={activeTabKey1}
      onTabChange={(key) => {
        onTab1Change(key);
      }}
    >
      {contentList[activeTabKey1]}
    </Card>
  );
}

export default MyInfo;
