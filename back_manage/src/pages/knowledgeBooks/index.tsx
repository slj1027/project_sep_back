import { FC, useEffect, useState } from 'react';
import style from './less/index.less';
import { useRequest } from 'ahooks';
import {
  _get_knowladge,
  _del_knowladge,
  _status_knowladge,
  _update_knowladge,
} from '@/api/knowladge';
import {
  EditOutlined,
  CloudUploadOutlined,
  SettingOutlined,
  DeleteOutlined,
} from '@ant-design/icons';
import {
  Button,
  Popconfirm,
  Empty,
  Input,
  Select,
  Form,
  Popover,
  Image,
  Spin,
  Breadcrumb,
} from 'antd';
import { Card } from 'antd';
import Knowladgemask from '@/components/knowladgemask';
import Pagination_com from '@/components/pagination_com';
import AffixHead from '@/components/affixHead';
import { history } from 'umi';
const { Meta } = Card;
const Knowladge: FC = () => {
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(12);
  const {
    data: knowladge,
    run,
    loading,
  } = useRequest((params = { page, pageSize }) => _get_knowladge(params));
  const [id, setId] = useState('');
  const [open, setOpen] = useState(false);
  const [type, setType] = useState('');
  const [obj, setObj] = useState({});
  const { Option } = Select;
  const [form] = Form.useForm();
  const [arr, setArr] = useState([]);
  useEffect(() => {
    setArr(knowladge?.data[0]);
  }, [knowladge]);
  const onFinish = (value: any) => {
    run({
      ...value,
      page,
      pageSize,
    });
  };
  const change = (page: number, pageSize: number) => {
    setPage(page);
    setPageSize(pageSize);
    run({
      page,
      pageSize,
    });
  };
  return (
    <div className={style.knowladgeWrap}>
      <AffixHead offsetTop={48}>
        <Breadcrumb>
          <Breadcrumb.Item>
            <a href="/work">工作台</a>
          </Breadcrumb.Item>
          <Breadcrumb.Item>
            <a href="/knowledge">知识小册</a>
          </Breadcrumb.Item>
        </Breadcrumb>
      </AffixHead>
      <main>
        <div className={style.know_select}>
          <Form
            form={form}
            name="control-hooks"
            onFinish={onFinish}
            layout="inline"
          >
            <Form.Item name="title" label="名称:" style={{ width: 300 }}>
              <Input />
            </Form.Item>
            <Form.Item name="status" label="状态:" style={{ width: 200 }}>
              <Select placeholder="">
                <Option value="已发布">已发布</Option>
                <Option value="草稿">草稿</Option>
              </Select>
            </Form.Item>
            <div className={style.know_button}>
              <Button type="primary" htmlType="submit">
                搜索
              </Button>
              <Button htmlType="reset">重置</Button>
            </div>
          </Form>
        </div>
        <div className={style.content}>
          <div className={style.card_content}>
            <div className={style.head}>
              <span>
                <Button
                  type="primary"
                  onClick={() => {
                    setOpen(true);
                    setType('new');
                  }}
                >
                  +新建
                </Button>
              </span>
            </div>
            {loading ? (
              <Spin />
            ) : (
              <>
                <div className={style.main}>
                  {knowladge && knowladge.data ? (
                    knowladge.data[0].map((item: any, index: number) => {
                      return (
                        <Card
                          key={index}
                          style={{ width: 300 }}
                          cover={
                            <Image
                              alt="example"
                              src={item.cover}
                              width="100%"
                              height={'150px'}
                            />
                          }
                          actions={[
                            <EditOutlined
                              onClick={() =>
                                history.push(`/knowledge/editor/${item.id}`)
                              }
                            />,
                            <Popover
                              content={
                                item.status === 'draft'
                                  ? '发布线上'
                                  : '设为草稿'
                              }
                            >
                              <CloudUploadOutlined
                                onClick={() => {
                                  if (item.status === 'draft') {
                                    _status_knowladge(item.id, {
                                      status: 'publish',
                                    });
                                    run({ page, pageSize });
                                  } else if (item.status === 'publish') {
                                    _status_knowladge(item.id, {
                                      status: 'draft',
                                    });
                                    run({ page, pageSize });
                                  }
                                }}
                              />
                            </Popover>,
                            <SettingOutlined
                              onClick={() => {
                                setObj(item);
                                setId(item.id);
                                setOpen(true);
                                setType('update');
                              }}
                            />,
                            <Popconfirm
                              title="确认删除这个分类？"
                              cancelText="取消"
                              okText="确认"
                              onConfirm={() => {
                                _del_knowladge(id).then(() => {
                                  run({ page, pageSize });
                                });
                              }}
                            >
                              <DeleteOutlined
                                onClick={() => {
                                  setId(item.id);
                                }}
                              />
                            </Popconfirm>,
                          ]}
                        >
                          <Meta
                            title={item.title}
                            description={item.summary}
                            style={{
                              display: 'flex',
                              flexDirection: 'column',
                              lineHeight: '40px',
                              justifyContent: 'space-around',
                              height: '80px',
                            }}
                          />
                        </Card>
                      );
                    })
                  ) : (
                    <Empty />
                  )}
                </div>
                <Pagination_com
                  pageSize={pageSize}
                  current={page}
                  onChange={change}
                  total={knowladge && knowladge.data && knowladge.data[1]}
                />
              </>
            )}
          </div>
          <div>
            <Knowladgemask
              open={open}
              onClose={() => {
                setOpen(false);
              }}
              type={type}
              id={id}
              obj={obj}
              run={run}
              page={page}
              pageSize={pageSize}
            ></Knowladgemask>
          </div>
        </div>
      </main>
    </div>
  );
};

export default Knowladge;
