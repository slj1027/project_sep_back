//action
import {getComment} from '@/api'
import { tableParams } from '@/types'
export const  getCommentList = (params:tableParams):any => {
    return async (dispatch:any) => {
        let res = await getComment(params)
        dispatch({
          type:'commentModel/GET_COMMENT',
          payload:res
        })
    }
}