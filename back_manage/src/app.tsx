import { RequestConfig, history, Link } from 'umi';
import { Menu, Dropdown, Avatar, Button } from 'antd';
import { UserOutlined, PlusOutlined } from '@ant-design/icons';
import CommonFooter from './components/CommonFooter';
import { getView } from './api';
import { useRequest } from 'ahooks';
interface HeadersType {
  authorization: string;
}
const menu = (
  <Menu
    items={[
      {
        key: '1',
        label: <a href="/myCenter">个人中心</a>,
      },
      {
        key: '2',
        label: <a href="/user">用户管理</a>,
      },
      {
        key: '3',
        label: <a href="/system">系统设置</a>,
      },
      {
        key: '4',
        label: (
          <span
            onClick={() => {
              localStorage.clear();
              history.push('/login');
            }}
          >
            退出登录
          </span>
        ),
      },
    ]}
  />
);
const menu1 = (
  <Menu
    items={[
      {
        key: '1',
        label: (
          <div
            onClick={(e) => {
              e.stopPropagation();
              history.push('/article/amEditor');
            }}
          >
            新建文章-协同编辑器
          </div>
        ),
      },
      {
        key: '2',
        label: (
          <div
            onClick={(e) => {
              e.stopPropagation();
              history.push('/article/editor');
            }}
          >
            新建文章
          </div>
        ),
      },
      {
        key: '3',
        label: (
          <div
            onClick={(e) => {
              e.stopPropagation();
              history.push('/page/editor');
            }}
          >
            新建页面
          </div>
        ),
      },
    ]}
  />
);

export const layout = () => {
  return {
    rightContentRender: () => {
      if (localStorage.getItem('user')) {
        return (
          <Dropdown overlay={menu} placement="bottomLeft">
            <div style={{ display: 'flex', alignItems: 'center' }}>
              {JSON.parse(localStorage.getItem('user') as string).avatar ? (
                <span
                  style={{
                    display: 'inline-block',
                    width: '30px',
                    height: '30px',
                    borderRadius: '15px',
                    marginRight: '20px',
                    overflow: 'hidden',
                    backgroundImage:
                      'url(' +
                      JSON.parse(localStorage.getItem('user') as string)
                        .avatar +
                      ')',
                    backgroundSize: 'cover',
                    backgroundPosition: '50%',
                    border: '1px solid #ccc',
                  }}
                ></span>
              ) : (
                <Avatar size="small" icon={<UserOutlined />} />
              )}

              {JSON.parse(localStorage.getItem('user') as string).name}
            </div>
          </Dropdown>
        );
      }
    },
    menuHeaderRender: (logo: string, title: string) => {
      const { data } = useRequest(getView);
      return (
        <div style={{ width: '100%' }}>
          <div>
            {logo}
            {title}
          </div>
          <Dropdown overlay={menu1} placement="bottom">
            <Button type="primary" style={{ width: '100%' }} className='add'>
              <PlusOutlined />
              <span className='add_button'>新建</span>
            </Button>
          </Dropdown>
        </div>
      );
    },
    footerRender: () => {
      return <CommonFooter />;
    },
  };
};
//*访问权限
export async function getInitialState() {
  try {
    let user = JSON.parse(localStorage.getItem('user') as string);
    return { ...user };
  } catch (error) {
    console.log(error);
  }
}
export const request: RequestConfig = {
  timeout: 50000,
  errorConfig: {
    adaptor: (resData) => {
      return {
        ...resData,
        errorMessage: resData.msg,
      };
    },
  },
  requestInterceptors: [
    (url, option) => {
      const headers = {
        ...option.headers,
      };
      //判断权限有没有
      if (option.isAuthorization) {
        (headers as HeadersType)['authorization'] = ('Bearer ' +
          localStorage.getItem('token')) as string;
      }
      return {
        url,
        option: {
          ...option,
          interceptors: true,
          ...headers,
        },
      };
    },
  ],
  responseInterceptors: [
    async (response) => {
      const res = await response.json();
      if (res.statusCode === 403) {
        res.msg = '你暂时没有权限访问或操作';
      }
      if (res.statusCode === 401) {
        history.replace('/login');
      }
      return res;
    },
  ],
};
