// 图表

export const getChartData = () =>
  new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        success: true,
        msg: '获取数据成功',
        data: [
          new Array(8).fill('').map(() => Math.floor(Math.random() * 100) + 1),
          new Array(8).fill('').map(() => Math.floor(Math.random() * 100) + 1),
        ],
      });
    }, 500);
  });
