
import { request } from "umi"

// 访问记录
export const _getVisit = (params = { page: 1, pageSize: 10 }) => request(`/api/api/view`, {
    method: "get",
    params,
    isAuthorization:true,
    headers: {
        authorization:
          'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
})

// 删除 访问记录
export const _delVisit = (id: string) => request(`/api/api/view/${id}`, {
    method: "delete",
    isAuthorization:true,
    headers: {
        authorization:
          'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });  