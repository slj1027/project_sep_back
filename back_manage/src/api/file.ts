import {request} from 'umi';
export const _upload = (body: FormData)=>request('/api/api/file/upload',{
    method:'post',
    data:body,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
})