import { request } from 'umi';

interface BasePageParams {
  pageSize: number;
  page: number;
}

export const _getUserList = (
  params: BasePageParams = { page: 1, pageSize: 10 },
) =>
  request(`/api/api/user`, {
    method: 'get',
    params,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
});
//禁用启用
export const _disableButton=(item:any)=>request(`/api/api/user/update`,{
    method:"POST",
    data:item,
    headers: {
        authorization:
          'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
})
//授权和解除授权
export const _shouButton=(item:any)=>request(`/api/api/user/update`,{
    method:"POST",
    data:item,
    headers: {
        authorization:
          'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
})

