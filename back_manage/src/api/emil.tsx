import { request } from 'umi';

// 删除
export const _del_emil = (id: string) =>
  request(`/api/api/smtp/${id}`, {
    method: 'DELETE',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
