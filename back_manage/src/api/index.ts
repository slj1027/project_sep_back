//api/api

import { tableParams, loginData, userData, updatePswData } from '@/types';
import { replyData, searchData } from '@/pages/comment/types'; //评论的类型校验
import { ossPosterData, searchPosterData } from '@/pages/poster/types'; //海报的类型校验
import { request } from 'umi';

export interface paramsType {
  page: number;
  pageSize: number;
}
//*登录
export const login = (data: loginData) =>
  request('/api/api/auth/login', {
    method: 'POST',
    data: data,
    isAuthorization: false,
  });
//*注册
export const register = (data: loginData) =>
  request('/api/api/user/register', {
    method: 'POST',
    data: data,
    isAuthorization: false,
  });
/* 
      -- 个人中心 --
*/
//*修改用户信息
export const updateUser = (data: userData) =>
  request('/api/api/user/update', {
    method: 'POST',
    data: data,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*更改密码
export const updatePsw = (data: updatePswData) =>
  request('/api/api/user/password', {
    method: 'POST',
    data: data,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
/* 
      -- 个人中心 --
*/
/* 
         -- 系统设置 -- 
*/
//*获取系统设置信息
export const getView = () =>
  request('/api/api/setting/get', {
    method: 'POST',
  });
export const setBase = (data: any) =>
  request('/api/api/setting', {
    method: 'POST',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const testSendEmail = (data: any) =>
  request('/api/api/smtp', {
    method: 'POST',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
/* 
            -- 系统设置 --
*/
//*获取文章管理列表
export const getArticle = (params: tableParams) =>
  request('/api/api/article', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
/* 
          -- 标题管理 --
*/
//*标题管理
export const getTags = () =>
  request('/api/api/tag', {
    method: 'get',
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*添加标题
export const addTags = (data: any) =>
  request('/api/api/tag', {
    method: 'post',
    isAuthorization: true,
    data,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*更改标题
export const updateTags = (id: string, data: any) =>
  request(`/api/api/tag/${id}`, {
    method: 'PATCH',
    isAuthorization: true,
    data,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*删除标题
export const delTags = (id: string) =>
  request(`/api/api/tag/${id}`, {
    method: 'delete',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
/* 
          -- 标题管理 --
*/
//*分类管理
export const getCategory = () =>
  request('/api/api/category', {
    method: 'get',
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*获取页面管理列表
export const getPage = (params: tableParams) =>
  request('/api/api/page', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*页面编辑
export const pageEditor = (id: string) =>
  request(`/api/api/page/${id}`, {
    method: 'get',
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*获取知识小册列表
export const getKnowledge = (params: tableParams) =>
  request('/api/api/knowledge', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
// --海报--
//*获取海报管理列表
export const getPoster = (params: tableParams) =>
  request('/api/api/poster', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const ossPoster = (data: ossPosterData) =>
  request('/api/api/poster', {
    method: 'post',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const searchPoster = (params: searchPosterData) =>
  request('/api/api/poster', {
    method: 'get',
    params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
// --海报--

/* 
  -- 评论页面 --
*/
//*获取评论列表
export const getComment = (params: searchData) =>
  request('/api/api/comment', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

//*通过/拒绝通过
export const postComment = (id: string, data: { pass: boolean }) =>
  request(`/api/api/comment/${id}`, {
    method: 'patch',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*回复评论
export const replyComment = (data: replyData) =>
  request(`/api/api/comment`, {
    method: 'post',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*删除评论
export const delComment = (id: string) =>
  request(`/api/api/comment/${id}`, {
    method: 'delete',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
/*
  -- 评论页面 --
*/
//*获取邮件管理列表
export const getEmail = (params: tableParams) =>
  request('/api/api/smtp', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
  export const delEmail = (id:string) =>
  request(`/api/api/smtp/${id}`, {
    method: 'DELETE',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*获取文件管理列表
export const getFile = (params: tableParams) =>
  request('/api/api/file', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*搜索记录
export const getSearch = (params: tableParams) =>
  request('/api/api/search', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//删除的数据
export const delserch = (id: string) =>
  request(`/api/api/search/${id}`, {
    method: 'delete',
    option: {
      isAuthorization: true,
    },
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

//*访问记录
export const getVisit = (params: tableParams) =>
  request('/api/api/view', {
    method: 'get',
    params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*用户记录
export const getUser = (params: tableParams) =>
  request('/api/api/user', {
    method: 'get',
    params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*最新文章
export const gitlist = (params: tableParams) =>
  request('/api/api/static', {
    method: 'get',
    params,
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

//*所有文章页
export const get_article = (params: paramsType = { page: 1, pageSize: 12 }) =>
  request('/api/api/article', {
    params,
    method: 'get',
    isAuthorization: false,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

//*首焦变撤销推荐
export const edit_article = (params: any) =>
  request(`/api/api/article/${params.id}`, {
    method: 'patch',
    data: params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*删除
export const del_article = (params: string) =>
  request(`/api/api/article/${params}`, {
    method: 'delete',
    data: params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

//*添加分类
export const add_category = (params: any) =>
  request('/api/api/category', {
    method: 'post',
    data: params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*删除分类
export const del_category = (params: any) =>
  request(`/api/api/category/${params}`, {
    method: 'delete',
    data: params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
//*编辑分类
export const edit_category = (params: any) =>
  request(`/api/api/category/${params.id}`, {
    method: 'patch',
    data: params,
    skipErrorHandler: true,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const get_tag = () =>
  request('/api/api/category', {
    method: 'get',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const addArticle = (data: any) =>
  request('/api/api/article', {
    method: 'POST',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const getArticleItem = (id: string) =>
  request(`/api/api/article/${id}`, {
    method: 'GET',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const setArticleItem = (id: string, data: any) =>
  request(`/api/api/article/${id}`, {
    method: 'PATCH',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const delArticleItem = (id: string) =>
  request(`/api/api/article/${id}`, {
    method: 'delete',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const addPage = (data: any) =>
  request('/api/api/page', {
    method: 'POST',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const getPageItem = (id: string) =>
  request(`/api/api/page/${id}`, {
    method: 'GET',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const setPageItem = (id: string, data: any) =>
  request(`/api/api/page/${id}`, {
    method: 'PATCH',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

export const delPageItem = (id: string) =>
  request(`/api/api/page/${id}`, {
    method: 'delete',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
