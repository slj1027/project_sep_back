import { request } from 'umi';
// 知识小册数据
export const _get_knowladge = (params: any) =>
  request(`/api/api/knowledge`, {
    method: 'get',
    params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const _get_knowladgeItem = (id: string) =>
  request(`/api/api/knowledge/${id}`, {
    method: 'get',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
// 删除
export const _del_knowladge = (id: string) =>
  request(`/api/api/knowledge/${id}`, {
    method: 'delete',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });

//添加
export const _add_knowladge = (params: any) =>
  request(`/api/api/Knowledge/book`, {
    method: 'post',
    data: params,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
// 更新
export const _update_knowladge = (id: string, obj: any) =>
  request(`/api/api/knowledge/${id}`, {
    method: 'patch',
    data: obj,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
// 更改文章状态
export const _status_knowladge = (id: string, obj: any) =>
  request(`/api/api/knowledge/${id}`, {
    method: 'patch',
    data: obj,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const _edit_knowladge = (data: any) =>
  request(`/api/api/knowledge/chapter`, {
    method: 'post',
    data,
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
export const _del_knowladgeChapter = (id: string) =>
  request(`/api/api/knowledge/${id}`, {
    method: 'delete',
    isAuthorization: true,
    headers: {
      authorization:
        'Bearer ' + JSON.parse(localStorage.getItem('token') as string),
    },
  });
